<?php
/** Forms for the admin menus
*/
/**	
 * @file admin_menu_forms.inc
 * @name admin_menu_forms 
 * @author Susanne Coates
 * @copyright (c)2015 University of Maryland
 * @todo
 */
/*
 This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License version 2
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

$modulePath = drupal_get_path('module','media_auto');
require_once $modulePath . '/src/MediaAuto.php';
require_once $modulePath . '/src/Utility.php';
require_once $modulePath . '/src/Views.php';
require_once $modulePath . '/src/MediaAutoTaxonomy.php';


function media_auto_admin_top_menu(){
  $menu = '<div id="generalSettings" class="topMenuItem"><a href="/admin/config/media/auto/general">General</a></div>'
      . '<div id="providerSettings" class="topMenuItem"><a href="/admin/config/media/auto/providers">Providers</a></div>'
      . '<div id="accountSettings" class="topMenuItem"><a href="/admin/config/media/auto/accounts">Accounts</a></div>'
      . '<div id="collectionSettings" class="topMenuItem"><a href="/admin/config/media/auto/collections">Collections</a></div>'
      . '<div id="indexedMediaSettings" class="topMenuItem"><a href="/admin/config/media/auto/media">Indexed Media</a></div>';
  return $menu;
}


// ********************************************************************
/** The admin settings forms
 * 
 * @param string array $form
 * @param string array $form_state
 * @return string array $form
 */
function media_auto_general_settings_form($form, &$form_state) {
    
    $form['#tree'] = true;
    $form['#attached']['css'] = array (
      drupal_get_path('module', 'media_auto') . '/css/media_auto.css'
    );
    
    $form['media_auto_general_settings'] = array(
      '#prefix' => '<div class="mediaAutoWrapper">',
      '#suffix' => '</div>',
    );
    $form['media_auto_general_settings']['menu'] = array(
      '#markup' => media_auto_admin_top_menu(),
      '#prefix' => '<div id="mediaAutoAdminTopMenu">',
      '#suffix' => '</div>',
    );
    $form['media_auto_general_settings']['table_label'] = array(
      '#markup' => '',
    );
    $form['media_auto_general_settings']['table_start'] = array(
      '#markup' => '<table class="settingsTable generalSettingsTable">',
    );
    $form['media_auto_general_settings']['table_header'] = array(
      '#markup' => '<tr class="tableHeader"><td>Parameter</td><td>Value</td></tr>',
    );

    $form['media_auto_general_settings']['auto_update_instructions'] = array(
      '#markup' => '',
      '#prefix' => '<tr class="elementRow"><td class="rowLabel"><h3>Auto-Update</h3></td><td class="cell">',
      '#suffix' => '</td></tr>',
    );
    $form['media_auto_general_settings']['auto_update'] = array(
      '#type' => 'checkbox',
      '#title' => t(''),
      '#description' => 'Automatically pull content from third-party providers.',
      '#default_value' => Utility::read_setting('auto_update'),
      '#prefix' => '<tr class="elementRow"><td class="rowLabel">Automatically Update Content?</td><td class="cell">',
      '#suffix' => '</td></tr>',
    );
    $form['media_auto_general_settings']['auto_update_interval'] = array(
      '#type' => 'textfield',
      '#title' => t(''),
      '#description' => 'How often to pull content (1=everytime <a href="/admin/config/system/cron">Drupal cron</a> runs, 2=every other time, etc.',
      '#default_value' => Utility::read_setting('auto_update_interval'),
      '#prefix' => '<tr class="elementRow"><td class="rowLabel">Content Update Frequency</td><td class="cell">',
      '#suffix' => '</td></tr>',
    );
    $form['media_auto_general_settings']['auto_unpublish_node'] = array(
      '#type' => 'checkbox',
      '#title' => t(''),
      '#description' => 'Automatically unpublish a node when it\'s linked media becomes unavailable on the provider.',
      '#default_value' => Utility::read_setting('auto_unpublish_node'),
      '#prefix' => '<tr class="elementRow"><td class="rowLabel">Auto-unpublish Node?</td><td class="cell">',
      '#suffix' => '</td></tr>',
    );
    $form['media_auto_general_settings']['auto_validate_interval'] = array(
      '#type' => 'textfield',
      '#title' => t(''),
      '#description' => 'How often to validate content (1=everytime <a href="/admin/config/system/cron">Drupal cron</a> runs, 2=every other time, etc.',
      '#default_value' => Utility::read_setting('auto_validate_interval'),
      '#prefix' => '<tr class="elementRow"><td class="rowLabel">Content Validation Frequency</td><td class="cell">',
      '#suffix' => '</td></tr>',
    );
    $form['media_auto_general_settings']['search_instructions'] = array(
      '#markup' => '',
      '#prefix' => '<tr class="elementRow"><td class="rowLabel"><h3>Queries<sup>&dagger;</sup></h3></td><td class="cell">',
      '#suffix' => '</td></tr>',
    );
    $form['media_auto_general_settings']['max_results'] = array(
      '#type' => 'textfield',
      '#title' => t(''),
      '#description' => 'The maximum number of search results to return per page (Maximum = 50)',
      '#default_value' => Utility::read_setting('max_results'),
      '#size' => 4,
      '#prefix' => '<tr class="elementRow"><td class="rowLabel">Max Results</td><td class="cell">',
      '#suffix' => '</td></tr>',
    );
    $form['media_auto_general_settings']['max_depth'] = array(
      '#type' => 'textfield',
      '#title' => t(''),
      '#description' => 'The maximum number of pages to return in a single search query on a provider',
      '#default_value' => Utility::read_setting('max_depth'),
      '#size' => 4,
      '#prefix' => '<tr class="elementRow"><td class="rowLabel">Max Pages</td><td class="cell">',
      '#suffix' => '</td></tr>',
    );
    
    $form['media_auto_general_settings']['debug_instructions'] = array(
      '#markup' => '',
      '#prefix' => '<tr class="elementRow"><td class="rowLabel"><h3>Debugging</h3></td><td class="cell">',
      '#suffix' => '</td></tr>',
    );
    $form['media_auto_general_settings']['debug'] = array(
      '#type' =>'checkbox',
      '#title' => '',
      '#description' => 'Prints verbose debugging output at the top of the page. NOTE: The <a href="https://www.drupal.org/project/devel">Devel Module</a> must be installed and enabled.',
      '#default_value' => Utility::read_setting('debug'),
      '#disabled' => !module_exists('devel'),
      '#prefix' => '<tr class="elementRow"><td class="rowLabel">Enable debugging output?</td><td class="cell">',
      '#suffix' => '</td></tr>',
    );
    $form['media_auto_general_settings']['log'] = array(
      '#type' =>'checkbox',
      '#title' => '',
      '#description' => 'Sends verbose activity logging to "Admininistration" > "Reports" > "Recent log messages" ',
      '#default_value' => Utility::read_setting('log'),
      '#prefix' => '<tr class="elementRow"><td class="rowLabel">Enable activity logging?</td><td class="cell">',
      '#suffix' => '</td></tr>',
    );
    
    $form['media_auto_general_settings']['table_end'] = array(
      '#markup' => '</table>',
    );
    
    $form['media_auto_general_settings']['submit'] = array (
      '#type' => 'submit',
      '#value' => 'Save Changes',
    );
    
    $form['media_auto_general_settings']['notes'] = array(
      '#markup' => '<br>&dagger; Query results returned by providers are paginated. '
      . 'You can limit the total number of results using Max Results and Max Pages (total results = max results x max pages). '
      . 'This limits the results of all queries (collections and media).',
    );
    if(Utility::read_setting('debug')){
      $query = db_select('views_display','v');
      $query->fields('v',array('vid','display_options'));
      $results = $query->execute();
      foreach($results as $result) {
        dpm(unserialize($result->display_options));
      }
    }
    return $form;
  }
 
function media_auto_general_settings_form_submit($form, $form_state) {
  Utility::write_setting('max_results',$form_state['values']['media_auto_general_settings']['max_results']);
  Utility::write_setting('max_depth',$form_state['values']['media_auto_general_settings']['max_depth']);
  Utility::write_setting('auto_update',$form_state['values']['media_auto_general_settings']['auto_update']);
  Utility::write_setting('auto_unpublish_node',$form_state['values']['media_auto_general_settings']['auto_unpublish_node']);
  Utility::write_setting('debug',$form_state['values']['media_auto_general_settings']['debug']);
  Utility::write_setting('log',$form_state['values']['media_auto_general_settings']['log']);
}
  
// ********************************************************************
/** The Provider settings form
 * 
 * @param string array $form
 * @param string array $form_state
 * @return string array $form
 */
  function media_auto_providers_settings_form($form, &$form_state) {
    //dpm(views_get_all_views());
    //dpm(node_type_get_types());
    //dpm(field_info_instances());
    
    $form['#tree'] = true;
    $form['#attached']['css'] = array (
      drupal_get_path('module', 'media_auto') . '/css/media_auto.css'
      );
    $form['media_auto_providers_settings'] = array(
      '#prefix' => '<div class="mediaAutoWrapper">',
      '#suffix' => '</div>',
    );
    $form['media_auto_providers_settings']['menu'] = array(
      '#markup' => media_auto_admin_top_menu(),
      '#prefix' => '<div id="mediaAutoAdminTopMenu">',
      '#suffix' => '</div>',
    );
    $form['media_auto_providers_settings']['table_label'] = array(
      '#markup' => '',
    );
    $form['media_auto_providers_settings']['table_start'] = array(
      '#markup' => '<table class="settingsTable providersSettingsTable">',
    );
    $form['media_auto_providers_settings']['table_header'] = array(
      '#markup' => '<tr class="tableHeader"><td>Provider ID</td><td>Auth Method</td><td>Auth Field 1</td><td>Auth Field 2</td><td>Auth Field 3</td><td>Auth Field 4</td><td>API Access URL</td><td>Watch URL</td><td>Branding URL</td></tr>',
    );
    $i=0;   
    $providers = MediaEnumeration::retrieveAllProviders();
    foreach($providers as $provider){
      $form['media_auto_providers_settings']['table_row'][$i] = array (
        '#prefix' => '<tr class="elementRow">',
        '#suffix' => '</tr>',
      );
    
      $form['media_auto_providers_settings']['table_row'][$i]['provider_id'] = array(
        '#type' => 'hidden',
        '#value' => $provider->getID(),
      );
      
      $form['media_auto_providers_settings']['table_row'][$i]['provider'] = array(
        '#markup' => $provider->getID(),
        '#prefix' => '<td class="cell">',
        '#suffix' => '</td>',
      );
      
      $form['media_auto_providers_settings']['table_row'][$i]['auth_method'] = array(
        '#type' => 'textfield',
        '#title' => t(''),
        '#default_value' => $provider->auth_method,
        '#size' => 8,
        '#prefix' => '<td class="cell">',
        '#suffix' => '</td>',
      );
      
      $form['media_auto_providers_settings']['table_row'][$i]['auth_field_1'] = array(
        '#type' => 'textfield',
        '#title' => t(''),
        '#default_value' => $provider->auth_field_1,
        '#size' => 32,
        '#prefix' => '<td class="cell">',
        '#suffix' => '</td>',
      );
      
      $form['media_auto_providers_settings']['table_row'][$i]['auth_field_2'] = array(
        '#type' => 'textfield',
        '#title' => t(''),
        '#default_value' => $provider->auth_field_2,
        '#size' => 32,
        '#prefix' => '<td class="cell">',
        '#suffix' => '</td>',
      );
      
      $form['media_auto_providers_settings']['table_row'][$i]['auth_field_3'] = array(
        '#type' => 'textfield',
        '#title' => t(''),
        '#default_value' => $provider->auth_field_3,
        '#size' => 32,
        '#prefix' => '<td class="cell">',
        '#suffix' => '</td>',
      ); 
      
      $form['media_auto_providers_settings']['table_row'][$i]['auth_field_4'] = array(
        '#type' => 'textfield',
        '#title' => t(''),
        '#default_value' => $provider->auth_field_4,
        '#size' => 32,
        '#prefix' => '<td class="cell">',
        '#suffix' => '</td>',
      );
          
      $form['media_auto_providers_settings']['table_row'][$i]['api_access_url'] = array(
        '#type' => 'textfield',
        '#title' => t(''),
        '#default_value' => $provider->api_access_url,
        '#size' => 32,
        '#prefix' => '<td class="cell">',
        '#suffix' => '</td>',
      );
     
      $form['media_auto_providers_settings']['table_row'][$i]['watch_url'] = array(
        '#type' => 'textfield',
        '#title' => t(''),
        '#default_value' => $provider->watch_url,
        '#size' => 32,
        '#prefix' => '<td class="cell">',
        '#suffix' => '</td>',
      );
      
      $form['media_auto_providers_settings']['table_row'][$i]['branding_url'] = array(
        '#type' => 'textfield',
        '#title' => t(''),
        '#default_value' => $provider->branding_url,
        '#size' => 32,
        '#prefix' => '<td class="cell">',
        '#suffix' => '</td>',
      );
      
      $i++;
      
      // store the count of the # of providers listed
      $form['media_auto_providers_settings']['provider_count'] = array (
        '#type' => 'hidden',
        '#value' => $i,
      );
    }
    unset($providers); // just to be sure the resources are freed.
    
    $form['media_auto_providers_settings']['table_end'] = array(
      '#markup' => '</table>',
    );
    
    $form['media_auto_providers_settings']['submit'] = array (
      '#type' => 'submit',
      '#value' => 'Save Changes',
    );
    
    $form['media_auto_providers_settings']['create_view'] = array (
      '#type' => 'submit',
      '#value' => 'Create View',
      '#submit' => array('create_view_callback'),
    );

    $form['media_auto_providers_settings']['settings_information'] = array(
      '#markup' => '
        <h3>Notes</h3>
        <p>
        When the <em>Auth Method</em> is "oauth2":
        <ul>
        <li><em>Auth Field 1</em> contains the Client Identifier</li>
        <li><em>Auth Field 2</em> contains the Client Secret</li>
        <li><em>Auth Field 3</em> contains the Access Token</li>
        <li><em>Auth Field 4</em> contains the URL to connect with the authorization server</li>
        </ul>
        </p>
        <p>
        When the <em>Auth Method</em> is "apikey":
        <ul>
        <li><em>Auth Field 1</em> contains the API Key</li>
        <li><em>Auth Field 2</em> is empty</li>
        <li><em>Auth Field 3</em> is empty</li>
        <li><em>Auth Field 4</em> is empty</li>
        </ul>
        </p>
      ',
      '#prefix' => '<div class="settingsInformation">',
      '#suffix' => '</div>',
    );
    return $form;
  }
  
  function create_view_callback($form, $form_state){
    
    Views::create_in_the_news_block();
    //Views::create_new_content_block();
  }
  /** Process the provider settings form
   * 
   * @param type $form
   * @param type $form_state
   */
  function media_auto_providers_settings_form_submit($form, &$form_state) {
    for($i=0;$i<$form_state['values']['media_auto_providers_settings']['provider_count'];$i++){
      // Get the data for the current provider from the database
      $provider_data = Utility::get_row('media_auto_providers', 'provider_id', $form_state['values']['media_auto_providers_settings']['table_row'][$i]['provider_id']);
      // Auth method
      ($form_state['values']['media_auto_providers_settings']['table_row'][$i]['auth_method'] <> $provider_data->auth_method)?
        Utility::update_cell(
            'media_auto_providers', 
            'provider_id', 
            $form_state['values']['media_auto_providers_settings']['table_row'][$i]['provider_id'], 
            'auth_method', 
            $form_state['values']['media_auto_providers_settings']['table_row'][$i]['auth_method']):'';
      // Auth field 1
      ($form_state['values']['media_auto_providers_settings']['table_row'][$i]['auth_field_1'] <> $provider_data->auth_field_1)?
        Utility::update_cell(
            'media_auto_providers', 
            'provider_id', 
            $form_state['values']['media_auto_providers_settings']['table_row'][$i]['provider_id'],
            'auth_field_1',
            $form_state['values']['media_auto_providers_settings']['table_row'][$i]['auth_field_1']):'';
      // Auth field 2
      ($form_state['values']['media_auto_providers_settings']['table_row'][$i]['auth_field_2'] <> $provider_data->auth_field_2)?
        Utility::update_cell(
            'media_auto_providers',
            'provider_id',
            $form_state['values']['media_auto_providers_settings']['table_row'][$i]['provider_id'],
            'auth_field_2',
            $form_state['values']['media_auto_providers_settings']['table_row'][$i]['auth_field_2']):'';
      // Auth field 3
      ($form_state['values']['media_auto_providers_settings']['table_row'][$i]['auth_field_3'] <> $provider_data->auth_field_3)?
        Utility::update_cell(
            'media_auto_providers', 
            'provider_id', 
            $form_state['values']['media_auto_providers_settings']['table_row'][$i]['provider_id'],
            'auth_field_3',
            $form_state['values']['media_auto_providers_settings']['table_row'][$i]['auth_field_3']):'';
      // Auth field 4
      ($form_state['values']['media_auto_providers_settings']['table_row'][$i]['auth_field_4'] <> $provider_data->auth_field_4)?
        Utility::update_cell(
            'media_auto_providers', 
            'provider_id', 
            $form_state['values']['media_auto_providers_settings']['table_row'][$i]['provider_id'],
            'auth_field_4',
            $form_state['values']['media_auto_providers_settings']['table_row'][$i]['auth_field_4']):'';  
      // API Access URL
      ($form_state['values']['media_auto_providers_settings']['table_row'][$i]['api_access_url'] <> $provider_data->api_access_url)?
        Utility::update_cell(
            'media_auto_providers', 
            'provider_id', 
            $form_state['values']['media_auto_providers_settings']['table_row'][$i]['provider_id'],
            'api_access_url',
            $form_state['values']['media_auto_providers_settings']['table_row'][$i]['api_access_url']):'';
      // Watch URL
      ($form_state['values']['media_auto_providers_settings']['table_row'][$i]['watch_url'] <> $provider_data->watch_url)?
        Utility::update_cell(
            'media_auto_providers',
            'provider_id',
            $form_state['values']['media_auto_providers_settings']['table_row'][$i]['provider_id'],
            'watch_url',
            $form_state['values']['media_auto_providers_settings']['table_row'][$i]['watch_url']):'';
      // Branding URL
      ($form_state['values']['media_auto_providers_settings']['table_row'][$i]['branding_url'] <> $provider_data->branding_url)?
        Utility::update_cell(
            'media_auto_providers', 
            'provider_id', 
            $form_state['values']['media_auto_providers_settings']['table_row'][$i]['branding_url'],
            'branding_url',
            $form_state['values']['media_auto_providers_settings']['table_row'][$i]['branding_url']):'';
    }
  }
  
  /* ------------------------------- */
  /** The Account settings form      */
  /* ------------------------------- */
  function media_auto_accounts_settings_form($form, &$form_state) { 
    MediaAuto::updateAccounts();

    $form['#tree'] = true;
    $form['#attached']['css'] = array (
      drupal_get_path('module', 'media_auto') . '/css/media_auto.css'
      );
    $form['media_auto_accounts_settings'] = array(
      '#prefix' => '<div class="mediaAutoWrapper">',
      '#suffix' => '</div>',
    );
    $form['media_auto_accounts_settings']['menu'] = array(
      '#markup' => media_auto_admin_top_menu(),
      '#prefix' => '<div id="mediaAutoAdminTopMenu">',
      '#suffix' => '</div>',
    );
    
    $form['media_auto_accounts_settings']['table_start'] = array(
      '#markup' => '<table class="settingsTable accountsSettingsTable">',
    );
    
    $form['media_auto_accounts_settings']['table_header'] = array(
      '#markup' => '<tr class="tableHeader">'
      . '<td class="accountCol">Account ID</td>'
      . '<td class="providerCol">Provider</td>'
      . '<td class="titleCol">Title</td>'
      . '<td class="descriptionCol">Description</td>'
      . '<td class="defaultThumbnailCol">Default</td>'
      . '<td class="largeThumbnailCol">Large</td>'
      . '<td class="smallThumbnailCol">Small</td>'
      . '<td class="enabledCol">&nbsp</td>'
      . '</tr>',
    );
    $i=0;    
    $accounts = MediaEnumeration::retrieveAllAccounts();
    foreach($accounts as $account){     
      $form['media_auto_accounts_settings'][$i] = array (
            '#prefix' => '<tr class="elementRow">',
            '#suffix' => '</tr>',
          );
      $form['media_auto_accounts_settings'][$i]['display_account_id'] = array(
        '#markup' => $account->getID(),
        '#prefix' => '<td class="cell accountCol">',
        '#suffix' => '</td>',
      );
      $form['media_auto_accounts_settings'][$i]['account_id'] = array(
        '#type' => 'hidden',
        '#value' => $account->getID(),
      );
      
      $form['media_auto_accounts_settings'][$i]['provider_id'] = array(
        '#markup' =>  (!$account->provider->branding_url)? $account->provider->getID() : '<img class="brandingThumbnail" src="'. $account->provider->branding_url . '">',
        '#prefix' => '<td class="cell providerCol">',
        '#suffix' => '</td>',
      );
      $form['media_auto_accounts_settings'][$i]['title'] = array(
        '#markup' => $account->title,
        '#prefix' => '<td class="cell titleCol">',
        '#suffix' => '</td>',
      );
      $form['media_auto_accounts_settings'][$i]['description'] = array(
        '#markup' => $account->description,
        '#prefix' => '<td class="cell descriptionCol">',
        '#suffix' => '</td>',
      );
      $form['media_auto_accounts_settings'][$i]['thumb_default'] = array(
        '#markup' => '<img class="imgThumbnail" src="'. $account->thumb_default .'">',
        '#prefix' => '<td class="cell defaultThumbnailCol">',
        '#suffix' => '</td>',
      );
      $form['media_auto_accounts_settings'][$i]['thumb_high'] = array(
        '#markup' => '<img class="imgThumbnail" src="'. $account->thumb_high .'">',
        '#prefix' => '<td class="cell largeThumbnailCol">',
        '#suffix' => '</td>',
      );
      $form['media_auto_accounts_settings'][$i]['thumb_low'] = array(
        '#markup' => '<img class="imgThumbnail" src="'. $account->thumb_low .'">',
        '#prefix' => '<td class="cell smallThumbnailCol">',
        '#suffix' => '</td>',
      );
      $form['media_auto_accounts_settings'][$i]['delete_account'] = array(
        '#type' => 'checkbox',
        '#title' => t('Delete'),
        '#default_value' => FALSE,
        '#prefix' => '<td class="cell enabledCol">',
        '#suffix' => '</td>',
      );
      $i++;
      $form['media_auto_accounts_settings']['i'] = array(
        '#type' => 'hidden',
        '#value' => $i,
        );
    }
    unset($accounts);// just to be sure the resources are freed.
    
    $form['media_auto_accounts_settings']['table_end'] = array(
      '#markup' => '</table>',
    );
    
    if($i<>0){
      $messageTemp = 'Deleting an account will delete all of it\'s associated channels, media and views! Are you sure you want to do this?';
      $form['media_auto_accounts_settings']['delete']['submit'] = array(
          '#type' => 'submit',
          '#value' => t('Delete Selected Accounts'),
          '#submit' => array('delete_accounts_callback'),
          '#attributes' => array('onclick' => 'if(!confirm("'. $messageTemp .'")){return false;}'),
          '#prefix' => '<div style="clear:both;">',
          '#suffix' => '</div><br>',
        );
    }
    $form['media_auto_accounts_settings']['add'] = array(
        '#prefix' => '<div class="addAccountGroup">',
        '#suffix' => '</div>',
      );
    $form['media_auto_accounts_settings']['add']['group_label'] = array(
        '#markup' => '<h3>Add an Account</h3>',
      );
    $form['media_auto_accounts_settings']['add']['account'] = array(
        '#type' => 'textfield',
        '#title' => t('Account ID'),
        '#size' => 20,
      );
    $form['media_auto_accounts_settings']['add']['provider'] = array(
        '#type' => 'select',
        '#title' => t('Provider'),
        '#options' => array('youtube'=>'YouTube','vimeo' => 'Vimeo'),
        '#multiple' => false,
      );
    $form['media_auto_accounts_settings']['add']['submit'] = array(
        '#type' => 'submit',
        '#submit' => array('add_account_callback'),
        '#value' => t('Add Account'),
      );
    $form['media_auto_accounts_settings']['message'] = array(
      '#markup' => 'With the exception of the Account ID and Provider, the information in the fields on this table are automatically obtained and updated from the third party platform (provider).',
    );
    
    return $form;
  }
 
  function add_account_callback($form, $form_state){
    $account = new Account();
    $account->setID($form_state['values']['media_auto_accounts_settings']['add']['account']);
    $account->provider->setID($form_state['values']['media_auto_accounts_settings']['add']['provider']);
    $account->provider->retrieve();
    $account->create();
    drupal_set_message('The account has been added to the system.');
  }
  
  function delete_accounts_callback($form, $form_state){
    for ($i=0;$i<$form_state['values']['media_auto_accounts_settings']['i'];$i++){
      $account = new Account();
      $account->setID($form_state['values']['media_auto_accounts_settings'][$i]['account_id']);
      $account->retrieve();
      if($form_state['values']['media_auto_accounts_settings'][$i]['delete_account']){
        MediaEnumeration::recursiveDeleteAccount($account);
        drupal_set_message('The account ' . $account->getID() . ', it\'s collections and all of their media, has been deleted.');
      }
      unset($account);
    }
  }
  
  function media_auto_collections_settings_form($form, &$form_state) {
    // display a list of all views
    (Utility::read_setting('debug'))?dpm(views_get_all_views()):'';
    
    $form['#tree'] = true;
    $form['#attached']['css'] = array (
      drupal_get_path('module', 'media_auto') . '/css/media_auto.css'
      );
    $form['media_auto_collections_settings'] = array(
      '#prefix' => '<div class="mediaAutoWrapper">',
      '#suffix' => '</div>',
    );
    $form['media_auto_collections_settings']['menu'] = array(
      '#markup' => media_auto_admin_top_menu(),
      '#prefix' => '<div id="mediaAutoAdminTopMenu">',
      '#suffix' => '</div>',
    );
    $form['media_auto_collections_settings']['channel_update_submit'] = array(
      '#type' => 'submit',
      '#value' => 'Update Collections Now',
      '#submit' => array ('collections_update_submit'),
    );
    
    $form['media_auto_collections_settings']['table_label'] = array(
      '#markup' => 'This table is automatically populated from the accounts listed on the Accounts admin page.',
    );
    $form['media_auto_collections_settings']['table_start'] = array(
      '#markup' => '<table class="settingsTable collectionsSettingsTable">',
    );

    $i=0;
    $accounts = MediaEnumeration::retrieveAllAccounts();
    foreach($accounts as $account){
      $j=0;
      $form['media_auto_collections_settings'][$i]['table_section_header'] = array(
      '#markup' => '<tr class="tableHeader tableSectionHeader"><td colspan="7"><div class="collectionLabel">' . $account->title . ' on</div>' . '<img class="brandingThumbnail" src="'. $account->provider->branding_url . '"></td></tr>',
      );
      $form['media_auto_collections_settings'][$i]['table_header'] = array(
        '#markup' => '<tr class="tableHeader">'
        //. '<td id="typeCol"></td>'
        . '<td class="titleCol">Title</td>'
        . '<td class="enabledCol">Enabled</td>'
        . '<td class="descriptionCol">Description</td>'
        . '<td class="publicationDateCol">Publication Date</td>'
        . '<td class="defaultThumbnailCol">Default</td>'
        . '<td class="largeThumbnailCol">Large</td>'
        . '<td class="smallThumbnailCol">Small</td>'
        . '</tr>',
      );
      $form['media_auto_collections_settings'][$i]['inner_table'] = array (
              '#prefix' => '<tr class="elementRow"><td colspan=7><div class="scrolledTableContainer"><table class="scrolledTable">',
              '#suffix' => '</table></div></td></tr>',
            );
      $collections = MediaEnumeration::retrieveAllCollections($account);
      foreach($collections as $collection){
        $modulePath = drupal_get_path('module','media_auto');
        //<table class="playlistTable"><tr class="elementRow"></tr></table>
        if(!empty($collection->parent)){
          $form['media_auto_collections_settings'][$i]['inner_table'][$j] = array (
            '#prefix' => '<tr class="playlistRow">',
            '#suffix' => '</tr>',
          );      
        } else {
          $form['media_auto_collections_settings'][$i]['inner_table'][$j] = array (
            '#prefix' => '<tr class="elementRow">',
            '#suffix' => '</tr>',
          );
        }
        $markupTemp = ($collection->enabled)?'<a href="/C'. $collection->getID() . '" target="_blank">'. $collection->title . '</a>': $collection->title;
        $form['media_auto_collections_settings'][$i]['inner_table'][$j]['title'] = array(
          '#markup' => $markupTemp,
          '#prefix' => '<td class="cell titleCol">',
          '#suffix' => '</td>',
        );
        
        $form['media_auto_collections_settings'][$i]['inner_table'][$j]['enabled'] = array(
          '#type' => 'checkbox',
          '#default_value' => $collection->enabled,
          '#multiple' => false,
          '#prefix' => '<td class="cell enabledCol">',
          '#suffix' => '</td>',
        );

        $form['media_auto_collections_settings'][$i]['inner_table'][$j]['collection_id'] = array(
          '#type' => 'hidden',
          '#value' => $collection->getID(),
        );

        $form['media_auto_collections_settings'][$i]['inner_table'][$j]['description'] = array(
        '#markup' => (strlen($collection->description)>120)?substr($collection->description,0,120) . '...':$collection->description,
        '#prefix' => '<td class="cell descriptionCol">',
        '#suffix' => '</td>',
        );
        
        $form['media_auto_collections_settings'][$i]['inner_table'][$j]['publication_date'] = array(
        '#markup' => $collection->publication_date,
        '#prefix' => '<td class="cell publicationDateCol">',
        '#suffix' => '</td>',
        );
        
        $form['media_auto_collections_settings'][$i]['inner_table'][$j]['thumb_default'] = array(
          '#markup' => '<img class="imgThumbnail" src="'. $collection->thumb_default . '">',
          '#prefix' => '<td class="cell defaultThumbnailCol">',
          '#suffix' => '</td>',
        );

        $form['media_auto_collections_settings'][$i]['inner_table'][$j]['thumb_high'] = array(
          '#markup' => '<img class="imgThumbnail" src="'. $collection->thumb_high . '">',
          '#prefix' => '<td class="cell largeThumbnailCol">',
          '#suffix' => '</td>',
        );

        $form['media_auto_collections_settings'][$i]['inner_table'][$j]['thumb_low'] = array(
          '#markup' => '<img class="imgThumbnail" src="'. $collection->thumb_low . '">',
          '#prefix' => '<td class="cell smallThumbnailCol">',
          '#suffix' => '</td>',
        );
        $j++;
      }
      unset($collections); // just to be sure the resources are freed.
      
      $form['media_auto_collections_settings'][$i]['channels'] = array(
          '#type' => 'hidden',
          '#value' => $j,
      );
      
      $i++;
    }
    unset($accounts); // just to be sure the resources are freed.
    $form['media_auto_collections_settings']['accounts'] = array(
        '#type' => 'hidden',
        '#value' => $i,
      );
    
    $form['media_auto_collections_settings']['table_end'] = array(
      '#markup' => '</table>',
    );
    
    $form['media_auto_collections_settings']['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Save Changes',
    );
    
    return $form;
  }
  
  /** Process the Collections Update
   * 
   * @param type $form
   * @param type $form_state
   */
  function collections_update_submit($form, $form_state){
    $mediaAuto = new MediaAuto();
    $mediaAuto->updateCollections();
    unset($mediaAuto);
    drupal_set_message('The Collection(s) have been updated.');
  }
  
  function media_auto_collections_settings_form_validate($form, $form_state){
    
    return $form;
  }
  
  /** Process the Collections settings.
   *  Enable/disable collections and create view when a collection is enabled
   * 
   * @param type $form
   * @param type $form_state
   */
  function media_auto_collections_settings_form_submit($form, $form_state){
    for($i=0;$i<$form_state['values']['media_auto_collections_settings']['accounts'];$i++){
      for($j=0;$j<$form_state['values']['media_auto_collections_settings'][$i]['channels'];$j++){
        $fields_array = array ('enabled' => $form_state['values']['media_auto_collections_settings'][$i]['inner_table'][$j]['enabled'], );
        Utility::update_row('media_auto_collections', 'collection_id',$form_state['values']['media_auto_collections_settings'][$i]['inner_table'][$j]['collection_id'] , $fields_array);
        $collection = new Collection();
        $collection->setID($form_state['values']['media_auto_collections_settings'][$i]['inner_table'][$j]['collection_id']);
        $collection->retrieve();
        // Create the view for the channel if enabled
        if($form_state['values']['media_auto_collections_settings'][$i]['inner_table'][$j]['enabled']){ 
          Views::create_feed_view($collection->title,substr($collection->description,0,250) . '...', $collection->getID());
          MediaEnumeration::changeCollectionMediaStatus($collection, 1);
        } else {
          Views::delete_feed_view($collection->title,$collection->getID());
          MediaEnumeration::changeCollectionMediaStatus($collection, 0);
        }
        
      }
    }
    drupal_set_message('The changes have been saved.');
  }
  
  /** The settings related to individual pieces of media
   * 
   * @param type $form
   * @param type $form_state
   * @return $form
   */
  function media_auto_media_settings_form($form, &$form_state) {
    
    $taxonomy_selections = Utility::build_media_auto_taxonomy_terms_selector();
    
    $form['#tree'] = true;
    $form['#attached']['css'] = array (
      drupal_get_path('module', 'media_auto') . '/css/media_auto.css'
      );
    $form['media_auto_media_settings'] = array(
      '#prefix' => '<div id="mediaAutoWrapper" class="mediaAutoWrapper">',
      '#suffix' => '</div>',
    );
    $form['media_auto_media_settings']['menu'] = array(
      '#markup' => media_auto_admin_top_menu(),
      '#prefix' => '<div id="mediaAutoAdminTopMenu">',
      '#suffix' => '</div>',
    );
    $form['media_auto_media_settings']['media_update_submit'] = array(
      '#type' => 'submit',
      '#value' => 'Update Media Now',
      '#submit' => array ('media_update_submit'),
    );
    $form['media_auto_media_settings']['table_label'] = array(
      '#markup' => '',
    );
    $form['media_auto_media_settings']['table_start'] = array(
      '#markup' => '<table class="settingsTable mediaSettingsTable">',
    );
    
    $i=0;
    $accounts = MediaEnumeration::retrieveAllAccounts();
    foreach($accounts as $account){
      $j=0;
      $collections = MediaEnumeration::retrieveAllCollections($account);
      foreach($collections as $collection){
        $k=0;
        if($collection->enabled){
          $link = '<a href="/C'. $collection->getID() . '" target="_blank">'. $collection->title . '</a>';
          $form['media_auto_media_settings'][$i][$j]['table_section_header'] = array(
            '#markup' => '<tr class="tableHeader tableSectionHeader"><td colspan="9">'
            . '<table class="innerTable"><tr>'
            . '<td class="cell"><img class="brandingThumbnail" src="'. $account->provider->branding_url . '"></td>'
            . '<td class="cell">' . $link . ' from '. $collection->account->title .'</td>'
            . '<td class="cell">' . ((!$collection->enabled)? '<span class="accountDisabledWarning">Updates are disabled for this channel</span>': '') . '</td>'
            . '</tr></table>'
            . '</td></tr>',
          );
          $form['media_auto_media_settings'][$i][$j]['table_header'] = array(
            '#markup' => '<tr class="tableHeader">'
            . '<td class="titleCol">Title</td>'
            . '<td class="descriptionCol">Description</td>'
            . '<td class="enabledCol">Enabled</td>'
            . '<td class="tagsCol">Tags</td>'
            . '<td class="publicationDateCol">Publication Date</td>'
            . '<td class="defaultThumbnailCol">Default</td>'
            . '<td class="largeThumbnailCol">Large</td>'
            . '<td class="smallThumbnailCol">Small</td>'
            . '</tr>',
          );
          $form['media_auto_media_settings'][$i][$j]['inner_table'] = array (
            '#prefix' => '<tr class="elementRow"><td colspan=8><div class="scrolledTableContainer"><table class="scrolledTable">',
            '#suffix' => '</table></div></td></tr>',
          );
          $mediaArray = MediaEnumeration::retrieveAllmedia($collection);
          foreach($mediaArray as $media){

            $form['media_auto_media_settings'][$i][$j]['inner_table'][$k] = array (
              '#prefix' => '<tr class="elementRow">',
              '#suffix' => '</tr>',
            );

            $form['media_auto_media_settings'][$i][$j]['inner_table'][$k]['media_id'] = array(
              '#type' => 'hidden',
              '#value' => $media->getID(),
            );
            
            // This fixes a formatting problem that occurs when people don't use 
            // spaces to separate words in the title. That is, when they type it as one long
            // phrase with no spaces or punctuation. In this case the title will
            // not wrap within the table cell.
            $temp = preg_replace('/[^a-z\d]+/i', ' ', $media->title);
            $words = str_word_count($temp);
            $title = ($words>=2)?$media->title:substr($media->title,0,24);  
            
            $form['media_auto_media_settings'][$i][$j]['inner_table'][$k]['title'] = array (
              '#markup' => '<a href="/node/' . $media->node->nid . '" target="_blank">' . $title . '</a>',
              '#prefix' => '<td class="cell titleCol">',
              '#suffix' => '</td>',
            );
            
            // Like the title, this filters punctuation in things like long URL's
            // replacing them with spaces so the description will wrap.
            $description = preg_replace('/[^a-z\d:().,!]+/i', ' ', $media->description);
            
            $form['media_auto_media_settings'][$i][$j]['inner_table'][$k]['description'] = array (
              '#markup' => ($description)?substr($description,0,240).' ...':'(No description available)',
              '#prefix' => '<td class="cell descriptionCol">',
              '#suffix' => '</td>',
            );   
            $form['media_auto_media_settings'][$i][$j]['inner_table'][$k]['published'] = array (
              '#type' => 'checkbox',
              '#default_value' => $media->node->status,
              '#ajax' => array (
                'callback' => 'media_auto_media_settings_form_submit',
                'wrapper' => 'mediaAutoWrapper',
                'event' => 'change',
                'method' => 'replace',
                ),
              '#prefix' => '<td class="cell enabledCol">',
              '#suffix' => '</td>',
            );
            $form['media_auto_media_settings'][$i][$j]['inner_table'][$k]['tags'] = array (
              '#type' => 'select',
              '#empty_option' => 'None',
              '#options' => $taxonomy_selections,
              '#default_value' => '',
              '#prefix' => '<td class="cell tagsCol">',
              '#suffix' => '</td>',
            );
            $form['media_auto_media_settings'][$i][$j]['inner_table'][$k]['publication_date'] = array(
              '#markup' => $media->publication_date,
              '#prefix' => '<td class="cell publicationDateCol">',
              '#suffix' => '</td>',
            );

            $form['media_auto_media_settings'][$i][$j]['inner_table'][$k]['thumb_default'] = array(
              '#markup' => '<img class="imgThumbnail" src="'. $media->thumb_default . '">',
              '#prefix' => '<td class="cell defaultThumbnailCol">',
              '#suffix' => '</td>',
            );

            $form['media_auto_media_settings'][$i][$j]['inner_table'][$k]['thumb_high'] = array(
              '#markup' => '<img class="imgThumbnail" src="'. $media->thumb_high . '">',
              '#prefix' => '<td class="cell largeThumbnailCol">',
              '#suffix' => '</td>',
            );

            $form['media_auto_media_settings'][$i][$j]['inner_table'][$k]['thumb_low'] = array(
              '#markup' => '<img class="imgThumbnail" src="'. $media->thumb_low . '">',
              '#prefix' => '<td class="cell smallThumbnailCol">',
              '#suffix' => '</td>',
            );  
            $k++;

            $form['media_auto_media_settings'][$i][$j]['media_count'] = array(
              '#type' => 'hidden',
              '#value' => $k,
            );
            unset($media);
          }
          $j++;
          $form['media_auto_media_settings'][$i]['channel_count'] = array(
              '#type' => 'hidden',
              '#value' => $j,
            );
/*
           $form['media_auto_media_settings'][$i]['spacer'] = array (
              '#prefix' => '<tr class="spacerRow"><td colspan="7">&nbsp;',
              '#suffix' => '</td></tr>',
            );
*/
          }
          unset($collection);
      }
      $i++;
      $form['media_auto_media_settings']['account_count'] = array(
            '#type' => 'hidden',
            '#value' => $i,
          );
      unset($account); // just to be sure the resources are freed.
    }
      
    $form['media_auto_media_settings']['table_end'] = array(
      '#markup' => '</table>',
    );  
    return $form;
  }
  
  function media_update_submit($form, $form_state){
    $mediaAuto = new MediaAuto();
    $mediaAuto->updateMedia();
    unset($mediaAuto);
    drupal_set_message('The media has been updated.');
  }
  
  function media_auto_media_settings_form_submit($form, $form_state){
    for($i=0;$i<$form_state['values']['media_auto_media_settings']['account_count'];$i++){
      for($j=0;$j<$form_state['values']['media_auto_media_settings'][$i]['channel_count'];$j++) {
        for($k=0;$k<$form_state['values']['media_auto_media_settings'][$i][$j]['media_count'];$k++){
          $media = new Media();
          $media->setID($form_state['values']['media_auto_media_settings'][$i][$j]['inner_table'][$k]['media_id']);
          $media->retrieve();
          if($media->status <> $form_state['values']['media_auto_media_settings'][$i][$j]['inner_table'][$k]['published']){
            $media->status = $form_state['values']['media_auto_media_settings'][$i][$j]['inner_table'][$k]['published'];
            $valueTemp = !(boolean)$form_state['values']['media_auto_media_settings'][$i][$j]['inner_table'][$k]['published'];
            Utility::update_cell('media_auto_media_index', 'media_id', $media->getID(), 'hide', (int)$valueTemp);
            $media->update();
          }
          $form_state['values']['media_auto_media_settings'][$i][$j]['inner_table'][$k]['tags'];
          
          // update the taxonomy terms
          /*
          $node->field_tags = array(
            'und'=> array (
            0 => array('tid'=>'1',),
            1 => array('tid'=>'2',),
            ),
           );
          */
          
          unset($media);
        }
      }
    }
  }