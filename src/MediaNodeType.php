<?php
/** A class of static functions to create/update/delete ContentTypes
*/
/**				  
 * @file ContentType.php
 * @name ContentType 
 * @author Susanne Coates
 * @copyright (c)2015 University of Maryland
 * @todo
 */
/*
 This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License version 2
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

require_once 'Utility.php';

interface MediaNodeTypeInterface{
  public function MediaNodeType();
  public function create();
  public function delete();
}

final class MediaNodeType implements MediaNodeTypeInterface {
  private $mediaTypeParams; // array to hold the media type definition
  private $bundle;
  
  /** MediaNodeType Class Constructor
   *  Sets the parameters for the new content type
   */
  public function MediaNodeType(){
    $this->mediaTypeParams = array(
      'type' => 'media',
      'name' => 'Media',
      'base' => 'node_content',
      'description' => t('Media content type used by the Media Auto module'),
      'custom' => 1,
      'modified' => 1,
      'locked' => 0,
    );
    $this->bundle = 'media';
  }
  
  /** Create the new content type
   *  Checks if the type exists, then saves it.
   */
  private function createMediaType(){
    if ( in_array( 'media', node_type_get_names() ) ) {
      drupal_set_message('The content type "media" already exists on the system','warning');
      return;
    }
    $this->mediaTypeParams = node_set_type_defaults( $this->mediaTypeParams );
    node_save_type( $this->mediaTypeParams );
    node_add_body_field( $this->mediaTypeParams );
  }

  private function createFieldCollection(){
    $field = array(
      'name' => 'collection',
      'type' => 'text'
    );
    if ( field_info_field( $field[ 'name' ] ) ) {
      drupal_set_message('The field "' . $field[ 'name' ] . '" already exists on the system','warning');
      return;
    }
    $field = field_create_field( $field );
    $fieldInstance = array(
      'field_name' => $field[ 'name' ],
      'entity_type' => 'node',
      'bundle' => $this->bundle,
      'description' => 'The unique identifier of the collection to which this media belongs',
      'label' => 'Collection ID',
      'widget' => array(
        'weight' => '11',
        'type' => 'textfield',
        'settings' => array ('size' => '60'),
      ),
      'display' => array(
        'default' => array(
          'label' => 'inline',
          'type' => 'hidden',
          'weight' => '7',
          'settings' => '',
        ),
        'full' => array(
          'label' => 'inline',
          'type' => 'hidden',
          'weight' => '7',
          'settings' => '',
        ),
        'teaser' => array(
          'label' => 'above',
          'type' => 'hidden',
          'weight' => '7',
          'settings' => '',
        ),
      ),
      
    );
    field_create_instance( $fieldInstance );
  }
  
  private function createFieldMid() {
    $field = array(
      'name' => 'mid',
      'type' => 'text'
    );
    if ( field_info_field( $field[ 'name' ] ) ) {
      drupal_set_message('The field "' . $field[ 'name' ] . '" already exists on the system','warning');
      return;
    }
    $field = field_create_field( $field );
    $fieldInstance = array(
      'field_name' => $field[ 'name' ],
      'entity_type' => 'node',
      'bundle' => $this->bundle,
      'description' => 'The unique identifier of the media.',
      'label' => 'Media ID',
      'widget' => array(
        'weight' => '0',
        'type' => 'textfield',
        'settings' => array ('size' => '60'),
      ),
      'display' => array(
        'default' => array(
          'label' => 'inline',
          'type' => 'hidden',
          'weight' => '8',
          'settings' => '',
        ),
        'full' => array(
          'label' => 'inline',
          'type' => 'hidden',
          'weight' => '8',
          'settings' => '',
        ),
        'teaser' => array(
          'label' => 'above',
          'type' => 'hidden',
          'weight' => '8',
          'settings' => '',
        ),
      ),
    );
    field_create_instance( $fieldInstance );
  }
  
  private function createFieldPublicationDate() {
    $field = array(
      'name' => 'publication_date',
      'type' => 'text'
    );
    if ( field_info_field( $field[ 'name' ] ) ) {
      drupal_set_message('The field "' . $field[ 'name' ] . '" already exists on the system','warning');
      return;
    }
    $field = field_create_field( $field );
    $fieldInstance = array(
      'field_name' => $field[ 'name' ],
      'entity_type' => 'node',
      'bundle' => $this->bundle,
      'description' => 'The the media was published on the provider.',
      'label' => 'Publication Date',
      'widget' => array(
        'weight' => '1',
        'type' => 'textfield',
        'settings' => array ('size' => '60'),
      ),
      'display' => array(
        'default' => array(
          'label' => 'inline',
          'type' => 'text_default',
          'weight' => '3',
          'settings' => '',
        ),
        'full' => array(
          'label' => 'above',
          'type' => 'text_default',
          'weight' => '3',
          'settings' => '',
        ),
        'teaser' => array(
          'label' => 'above',
          'type' => 'hidden',
          'weight' => '9',
          'settings' => '',
        ),
      ),
    );
    field_create_instance( $fieldInstance );
  }
  
  private function createFieldThumbnailLarge() {
    $field = array(
      'name' => 'thumbnail_large',
      'type' => 'text'
    );
    if ( field_info_field( $field[ 'name' ] ) ) {
      drupal_set_message('The field "' . $field[ 'name' ] . '" already exists on the system','warning');
      return;
    }
    $field = field_create_field( $field );
    $fieldInstance = array(
      'field_name' => $field[ 'name' ],
      'entity_type' => 'node',
      'bundle' => $this->bundle,
      'description' => 'Large thumbnail image',
      'label' => 'Large Thumbnail',
      'widget' => array(
        'weight' => '8',
        'type' => 'textfield',
        'settings' => array ('size' => '60'),
      ),
      'display' => array(
        'default' => array(
          'label' => 'inline',
          'type' => 'hidden',
          'weight' => '9',
          'settings' => '',
        ),
        'full' => array(
          'label' => 'inline',
          'type' => 'hidden',
          'weight' => '9',
          'settings' => '',
        ),
        'teaser' => array(
          'label' => 'above',
          'type' => 'hidden',
          'weight' => '6',
          'settings' => '',
        ),
      ),
    );
    field_create_instance( $fieldInstance );
  }
  
  private function createFieldThumbnailSmall() {
    $field = array(
      'name' => 'thumbnail_small',
      'type' => 'text'
    );
    if ( field_info_field( $field[ 'name' ] ) ) {
      drupal_set_message('The field "' . $field[ 'name' ] . '" already exists on the system','warning');
      return;
    }
    $field = field_create_field( $field );
    $fieldInstance = array(
      'field_name' => $field[ 'name' ],
      'entity_type' => 'node',
      'bundle' => $this->bundle,
      'description' => 'Small thumbnail image.',
      'label' => 'Small Thumbnail',
      'widget' => array(
        'weight' => '9',
        'type' => 'textfield',
        'settings' => array ('size' => '60'),
      ),
      'display' => array(
        'default' => array(
          'label' => 'inline',
          'type' => 'hidden',
          'weight' => '10',
          'settings' => '',
        ),
        'full' => array(
          'label' => 'inline',
          'type' => 'hidden',
          'weight' => '10',
          'settings' => '',
        ),
        'teaser' => array(
          'label' => 'above',
          'type' => 'hidden',
          'weight' => '5',
          'settings' => '',
        ),
      ),
    );
    field_create_instance( $fieldInstance );
  }
  
  private function createFieldThumbnailDefault() {
    $field = array(
      'name' => 'thumbnail_default',
      'type' => 'text'
    );
    if ( field_info_field( $field[ 'name' ] ) ) {
      drupal_set_message('The field "' . $field[ 'name' ] . '" already exists on the system','warning');
      return;
    }
    $field = field_create_field( $field );
    $fieldInstance = array(
      'field_name' => $field[ 'name' ],
      'entity_type' => 'node',
      'bundle' => $this->bundle,
      'description' => 'Default thumbnail image.',
      'label' => 'Default Thumbnail',
      'widget' => array(
        'weight' => '10',
        'type' => 'textfield',
        'settings' => array ('size' => '60'),
      ),
      'display' => array(
        'default' => array(
          'label' => 'inline',
          'type' => 'hidden',
          'weight' => '6',
          'settings' => '',
        ),
        'full' => array(
          'label' => 'inline',
          'type' => 'hidden',
          'weight' => '6',
          'settings' => '',
        ),
        'teaser' => array(
          'label' => 'above',
          'type' => 'hidden',
          'weight' => '2',
          'settings' => '',
        ),
      ),
    );
    field_create_instance( $fieldInstance );
  }
  
  private function createFieldMediaUrl() {
    $field = array(
      'name' => 'media_url',
      'type' => 'text'
    );
    if ( field_info_field( $field[ 'name' ] ) ) {
      drupal_set_message('The field "' . $field[ 'name' ] . '" already exists on the system','warning');
      return;
    }
    $field = field_create_field( $field );
    $fieldInstance = array(
      'field_name' => $field[ 'name' ],
      'entity_type' => 'node',
      'bundle' => $this->bundle,
      'description' => 'Media URL',
      'label' => 'The link to the media on the provider',
      'widget' => array(
        'weight' => '5',
        'type' => 'media_generic',
        'module' => 'media',
        'settings' => array (
          'browser_plugins' => array(
            'media_internet' => 'media_internet',
            'upload' => '0',
            'media_default--media_browser_1' => '0',
            'media_default--media_browser_my_files' => '0',
          ),
          'allowed_types' => array(
            'video' => 'video',
            'image' => '0',
            'audio' => '0',
            'document' => '0',
          ),
          'allowed_schemes' => array(
            'oembed' => 'oembed',
            'public' => 'public',
          ),
        ),
      ),
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'type' => 'file_rendered',
          'weight' => '0',
          'settings' => array('file_view_mode' => 'default',),
          'module' => 'file_entity',
        ),
        'full' => array(
          'label' => 'hidden',
          'type' => 'file_rendered',
          'weight' => '0',
          'settings' => array('file_view_mode' => 'default',),
          'module' => 'file_entity',
        ),
        'teaser' => array(
          'label' => 'above',
          'type' => 'hidden',
          'weight' => '3',
          'settings' => '',
        ),
      ),
    );
    field_create_instance( $fieldInstance );
  }
  
  private function createFieldFeedItemDescription() {
    $field = array(
      'name' => 'feed_item_description',
      'type' => 'text'
    );
    if ( field_info_field( $field[ 'name' ] ) ) {
      drupal_set_message('The field "' . $field[ 'name' ] . '" already exists on the system','warning');
      return;
    }
    $field = field_create_field( $field );
    $fieldInstance = array(
      'field_name' => $field[ 'name' ],
      'entity_type' => 'node',
      'bundle' => $this->bundle,
      'description' => 'Description from the provider.',
      'label' => 'Description',
      'widget' => array(
        'weight' => '4',
        'type' => 'text_textarea_with_summary',
        'module' =>'text',
        'settings' => array (
          'rows' => '10',
          'summary_rows' =>'5',
        ), 
      ),
      'display' => array(
        'default' => array(
          'label' => 'above',
          'type' => 'text_default',
          'weight' => '4',
          'settings' => '',
          'module' =>'text',
        ),
        'teaser' => array(
          'label' => 'hidden',
          'type' => 'text_summary_or_trimmed',
          'weight' => '0',
          'settings' => array(
            'trim_length' => '600',
          ),
          'module' =>'text',
        ),
        'full' => array(
          'label' => 'hidden',
          'type' => 'text_default',
          'weight' => '1',
          'settings' => '',
          'module' =>'text',
        ),
      ),
    );
    field_create_instance( $fieldInstance );
  }
  
  private function createFieldTags() {
    $field = array(
      'name' => 'tags',
      'type' => 'text'
    );
    if ( field_info_field( $field[ 'name' ] ) ) {
      drupal_set_message('The field "' . $field[ 'name' ] . '" already exists on the system','warning');
      return;
    }
    $field = field_create_field( $field );
    $fieldInstance = array(
      'field_name' => $field[ 'name' ],
      'entity_type' => 'node',
      'bundle' => $this->bundle,
      'description' => 'Taxonomy terms to be associated with this media',
      'label' => 'Tags',
      'widget' => array(
        'weight' => '13',
        'type' => 'options_select',
        'module' =>'options',
        'settings' => '', 
      ),
      'display' => array(
        'default' => array(
          'label' => 'above',
          'type' => 'taxonomy_term_reference_link',
          'weight' => '11',
          'settings' => '',
          'module' =>'taxonomy',
        ),
        'teaser' => array(
          'label' => 'hidden',
          'type' => 'above',
          'weight' => '0',
          'settings' => '',
        ),
        'full' => array(
          'label' => 'above',
          'type' => 'hidden',
          'weight' => '0',
          'settings' => '',
        ),
      ),
    );
    field_create_instance( $fieldInstance );
  }

  
  public function create() {
    $this->createMediaType(); 
    $this->createFieldChannel();
    $this->createFieldMid();
    $this->createFieldPublicationDate();
    $this->createFieldThumbnailLarge();
    $this->createFieldThumbnailSmall();
    $this->createFieldThumbnailDefault();
    $this->createFieldMediaUrl();
    $this->createFieldFeedItemDescription();
    $this->createFieldTags();
  }
  
  public function delete(){

    // delete the fields
    // delete the type
  }
  
}
