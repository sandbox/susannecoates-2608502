<?php
/** Utility Functions
*/
/**				  
 * @file Utility.php
 * @name Utility 
 * @author Susanne Coates
 * @copyright (c)2015 University of Maryland
 * @todo
 */
/*
 This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License version 2
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

interface UtilityInterface {
  public static function read_setting($variable_name);
  public static function write_setting($parameter, $value);
  public static function get_rows($table, $searchColumn, $searchValue);
  public static function get_row($table, $searchColumn, $searchValue);
  public static function update_cell($table, $searchColumn, $searchValue, $column, $value);
  public static function update_row($table, $searchColumn, $searchValue, $fields_array);
  public static function insert_row($table, $fields_array);
  public static function insert_autoindexed_row($table, $fields_array);
  public static function drop_row($table, $searchColumn, $searchValue);
  public static function ping($host);
  public static function build_media_auto_taxonomy_terms_selector();
}

final class Utility implements UtilityInterface {
  
  public static function read_setting($variable_name) {
    $query = db_select('media_auto_settings', 'a')
      ->fields('a', array('value'))
      ->condition('parameter',$variable_name,'='); 
    $result = $query->execute()->fetch();
    return ($result) ? $result->value : NULL;
  }

  public static function write_setting($parameter, $value) {
    $query = db_update('media_auto_settings')
    ->fields(array('value' => $value))
    ->condition('parameter', $parameter, '=');
    return $query->execute();
  }

  /* *************************************** */
  /* GENERIC UPDATE, DROP AND INSERT QUERIES */
  /* *************************************** */

  /** Get multiple rows from a table that match the search creteria */
  public static function get_rows($table, $searchColumn, $searchValue) {
    $query = db_select($table, 'x')
      ->fields('x')
      ->condition($searchColumn,$searchValue,'='); 
    return $query->execute();
  }

  /** Get multiple rows from a table that match the search creteria */
  public static function get_row($table, $searchColumn, $searchValue) {
    $query = db_select($table, 'x')
      ->fields('x')
      ->condition($searchColumn,$searchValue,'='); 
    return $query->execute()->fetch();
  }
  
  /** update a single cell from a table */
  public static function update_cell($table, $searchColumn, $searchValue, $column, $value) {
    // update a single cell in a table
    $query = db_update($table)
      ->fields(array($column => $value))
      ->condition($searchColumn, $searchValue);

    //$return = FALSE;
    $return = $query->execute();
    // Print the debugging information if enabled
    if(module_exists('devel') && Utility::read_setting('debug')) {
      $debug_ar['function'] = 'update_cell';
      $debug_ar['Parameter: table'] = $table;
      $debug_ar['Parameter: search_column'] = $searchColumn;
      $debug_ar['Parameter: search_value'] = $searchValue;
      $debug_ar['Parameter: column'] = $column;
      $debug_ar['Parameter: value'] = $value;
      $debug_ar['Return'] = $return;
      dpm($debug_ar, $name='UPDATED_CELL');	 
    }
    return $return;
  }

  /** update an entire row on a table */
  public static function update_row($table, $searchColumn, $searchValue, $fields_array) {
      $query = db_update($table)
      ->fields($fields_array)
      ->condition($searchColumn, $searchValue);
      $return = $query->execute();
      
      // Print the debugging information if enabled
    if(module_exists('devel') && Utility::read_setting('debug')) {
      $debug_ar['function'] = 'update_row';
      $debug_ar['Parameter: table'] = $table;
      $debug_ar['Parameter: search_column'] = $searchColumn;
      $debug_ar['Parameter: search_value'] = $searchValue;
      $debug_ar['Parameter: fields_array'] = $fields_array;
      $debug_ar['Return'] = $return;
      dpm($debug_ar, $name='UPDATED_ROW');	 
    }
  }

  /** update a single row on a table return the last insert ID for autoindexed rows o/w query result. */
  public static function insert_row($table, $fields_array) {
    $return = array();

    // update a single cell in a table
    $query = db_insert($table);
    $query->fields($fields_array);
    $return = $query->execute();

    // Print the debugging information if enabled
    if(module_exists('devel') && Utility::read_setting('debug')) {
      $debug_ar['function'] = 'insert_row';
      $debug_ar['Parameter: table'] = $table;
      $debug_ar['Parameter: columns'] = $fields_array;
      $debug_ar['Return'] = $return;
      dpm($debug_ar, $name='INSERTED_ROW');	 
    }
    return $return;
  }

  /** update a single row on a table return the last insert ID for autoindexed rows o/w query result. */
  public static function insert_autoindexed_row($table, $fields_array) {
    $return = array();

    // update a single cell in a table
    $query = db_insert($table);
    $query->fields($fields_array);
    $query->execute();

    $query = db_select($table, 'x');	
    $query->addExpression('LAST_INSERT_ID()', 'id' );
    $return = $query->execute()->fetch()->id;

    // Print the debugging information if enabled
    if(module_exists('devel') && Utility::read_setting('debug')) {
      $debug_ar['function'] = 'insert_row';
      $debug_ar['Parameter: table'] = $table;
      $debug_ar['Parameter: columns'] = $fields_array;
      $debug_ar['Return'] = $return;
      dpm($debug_ar, $name='INSERTED_ROW');	 
    }

    return $return;
  }

  /** Drop a row from a table 
  */
  public static function drop_row($table, $searchColumn, $searchValue) {
    $query = db_delete($table);
    $query->condition($searchColumn, $searchValue, '=');
    $return = $query->execute();

    // Print the debugging information if enabled	
    if(module_exists('devel') && Utility::read_setting('debug')) {
      $debug_ar['function'] = 'drop_row';
      $debug_ar['Parameter: table'] = $table;
      $debug_ar['Parameter: search_column'] = $searchColumn;
      $debug_ar['Parameter: search_value'] = $searchValue;
      $debug_ar['Return value'] = $return;
      dpm($debug_ar, $name='DROPPED_ROW');	 
    }
    return $return;
  }
  
  /** Validation Functions */
  public static function ping($host, $timeout = 1) {
    /* ICMP ping packet with a pre-calculated checksum */
    $package = "\x08\x00\x7d\x4b\x00\x00\x00\x00PingHost";
    $socket = socket_create(AF_INET, SOCK_RAW, 1);
    socket_set_option($socket, SOL_SOCKET, SO_RCVTIMEO, array('sec' => $timeout, 'usec' => 0));
    socket_connect($socket, $host, null);

    $ts = microtime(true);
    socket_send($socket, $package, strLen($package), 0);
    if (socket_read($socket, 255)) {    
        $result = microtime(true) - $ts;
    } else {
        $result = false;
    }
    socket_close($socket);
    
    // Print the debugging information if enabled
    if(module_exists('devel') && Utility::read_setting('debug')) {
      $debug_ar['Function:'] = 'ping';
      $debug_ar['Parameter: host'] = $host;
      $debug_ar['Parameter: timeout'] = $timeout;
      $debug_ar['Return value'] = $result;
      dpm($debug_ar, $name='UPDATED_CELL');	 
    }

    return $result;
  }
 
  public static function build_media_auto_taxonomy_terms_selector(){
    $selections = array();
    $vocabulary = taxonomy_vocabulary_machine_name_load( 'media_auto_tags' );
    $vterms = entity_load( 'taxonomy_term', FALSE, array( 'vid' => $vocabulary->vid ));
    foreach($vterms as $vterm){
      $selections[$vterm->tid] = $vterm->name;
    }
    return $selections;
  }
  
}



