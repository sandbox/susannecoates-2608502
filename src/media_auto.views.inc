<?php
/**		
 * @file media_auto_views.php
 * @name media_auto_views 
 * @version 7.x-1.x		  
 * @author Susanne Coates
 * @copyright (c) 2015 by Susanne Coates, University of Maryland, College of Agriculture and Natural Resources
 * @todo
 */
/*
 This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License version 2
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
function media_auto_views_data() {

    /* **********************************************************
        The <media_auto_collections> TABLE
       ********************************************************** */
    $data['media_auto_collections']['table']['group'] = t('Collections Table');
    $data['media_auto_collections']['table']['base'] = array(
        'field' => 'collection_id',
        'title'=>t('Media Collections'),
        'help'=>t('Contains Collections table columns we want exposed to Views.'),
        'weight' => -10,
        );

    $data['media_auto_collections']['collection_id'] = array(
        'title' => t('Collection ID'),
        'help' => t('The unique ID for the collection.'),
        'field' => array(
                'handler' => 'views_handler_field',
                ),
                'sort' => array(
                'handler' => 'views_handler_sort',
        ),
        'filter' => array(
                'handler' => 'views_handler_filter_string',
        ),		
    );
    
    $data['media_auto_collections']['account_id'] = array(
        'title' => t('Account ID'),
        'help' => t('The unique ID for the account associated with this collection.'),
        'field' => array(
                'handler' => 'views_handler_field',
                ),
                'sort' => array(
                'handler' => 'views_handler_sort',
        ),
        'filter' => array(
                'handler' => 'views_handler_filter_string',
        ),		
    );
    
    $data['media_auto_collections']['etag'] = array(
        'title' => t('E-Tag'),
        'help' => t('The worldwide unique ID for this piece of media.'),
        'field' => array(
                'handler' => 'views_handler_field',
                ),
                'sort' => array(
                'handler' => 'views_handler_sort',
        ),
        'filter' => array(
                'handler' => 'views_handler_filter_string',
        ),		
    );
    
    $data['media_auto_collections']['title'] = array(
        'title' => t('Collection Title'),
        'help' => t('The title for this collection'),
        'field' => array(
                'handler' => 'views_handler_field',
                ),
                'sort' => array(
                'handler' => 'views_handler_sort',
        ),
        'filter' => array(
                'handler' => 'views_handler_filter_string',
        ),		
    );
    
    $data['media_auto_collections']['description'] = array(
        'title' => t('Collection Description'),
        'help' => t('Collection description.'),
        'field' => array(
                'handler' => 'views_handler_field',
                ),
                'sort' => array(
                'handler' => 'views_handler_sort',
        ),
        'filter' => array(
                'handler' => 'views_handler_filter_string',
        ),		
    );
    
    $data['media_auto_collections']['type'] = array(
        'title' => t('Type'),
        'help' => t('The collection type'),
        'field' => array(
                'handler' => 'views_handler_field',
                ),
                'sort' => array(
                'handler' => 'views_handler_sort',
        ),
        'filter' => array(
                'handler' => 'views_handler_filter_string',
        ),		
    );
    
    $data['media_auto_collections']['publication_date'] = array(
        'title' => t('Publication Date'),
        'help' => t('The date this collection was first published on the provider'),
        'field' => array(
                'handler' => 'views_handler_field',
                ),
                'sort' => array(
                'handler' => 'views_handler_sort',
        ),
        'filter' => array(
                'handler' => 'views_handler_filter_string',
        ),		
    );
    
    $data['media_auto_collections']['thumb_default'] = array(
        'title' => t('Default Thumbnail'),
        'help' => t('The default thumbnail image for the collection.'),
        'field' => array(
                'handler' => 'views_handler_field_user_picture',
                ),
                'sort' => array(
                'handler' => 'views_handler_sort',
        ),
        'filter' => array(
                'handler' => 'views_handler_filter_string',
        ),		
    );
    
    $data['media_auto_collections']['thumb_high'] = array(
        'title' => t('Large Thumbnail'),
        'help' => t('The large thumbnail image for the collection.'),
        'field' => array(
                'handler' => 'views_handler_field_user_picture',
                ),
                'sort' => array(
                'handler' => 'views_handler_sort',
        ),
        'filter' => array(
                'handler' => 'views_handler_filter_string',
        ),		
    );
    
    $data['media_auto_collections']['thumb_low'] = array(
        'title' => t('Small Thumbnail'),
        'help' => t('The small thumbnail image for the collection.'),
        'field' => array(
                'handler' => 'views_handler_field_user_picture',
                ),
                'sort' => array(
                'handler' => 'views_handler_sort',
        ),
        'filter' => array(
                'handler' => 'views_handler_filter_string',
        ),		
    );
   
    $data['media_auto_collections']['enabled'] = array(
        'title' => t('Collection Enabled?'),
        'help' => t(''),
        'field' => array(
                'handler' => 'views_handler_field',
                ),
                'sort' => array(
                'handler' => 'views_handler_sort',
        ),
        'filter' => array(
                'handler' => 'views_handler_filter_string',
        ),		
    );
    
    $data['media_auto_collections']['parent'] = array(
        'title' => t('Parent'),
        'help' => t('The parent\'s ID if this is a sub-collection'),
        'field' => array(
                'handler' => 'views_handler_field',
                ),
                'sort' => array(
                'handler' => 'views_handler_sort',
        ),
        'filter' => array(
                'handler' => 'views_handler_filter_string',
        ),		
    );
    
    return $data;
	
}
