<?php
/** A class of static functions to create/update/delete Views
*/
/**				  
 * @file Views.php
 * @name Views 
 * @author Susanne Coates
 * @copyright (c)2015 University of Maryland
 * @todo
 */
/*
 This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License version 2
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

require_once 'Utility.php';

interface ViewsInterface {
  public static function create_feed_view($view_name,$view_description,$view_id);
  public static function delete_feed_view($view_name,$view_id);
  public static function create_new_content_block();
  public static function create_recent_channels_block();
  public static function create_featured_content_block();
  public static function delete_view($name);
}

final class Views implements ViewsInterface {
  
  public static function create_feed_view($view_name,$view_description,$view_id){
    $query = db_select('views_view', 'v');
    $query->fields('v',array('tag'));
    $query->condition('tag',$view_id,'=');
    $result = $query->execute()->fetch();
    if(empty($result)) {
      /* Create the view and set some basic information */
      $view = views_new_view();
      $acn = (strlen($view_id) > 10)?substr($view_id, 0, 10):$view_id;
      $view->name = $view_name . '_' . $acn;
      $view->description = $view_description;
      $view->tag = $view_id;
      $view->view_php = '';
      $view->base_table = 'node';
      $view->core = '7';
      $view->api_version = '3';
      $view->is_cacheable = FALSE;
      $view->disabled = FALSE;

      /* Create the display and populate the options */
      $handler = $view->new_display('default', 'Defaults', 'default');
      $handler->display->display_options['title'] = $view_name;
      $handler->display->display_options['access']['type'] = 'role';
      $handler->display->display_options['access']['role'] = array( '3' => '3', );  
      $handler->display->display_options['cache']['type'] = 'none';
      $handler->display->display_options['exposed_form']['type'] = 'basic';
      $handler->display->display_options['css_class'] = 'media-auto-media-feed';
      $handler->display->display_options['pager'] = array (
        'type' => 'full',
        'options' => array (
          'items_per_page' => '10',
          'offset' => '0',
          'id' => '0',
          ),
        );

      /* Handler */
      $handler->display->display_options['style_plugin'] = 'grid';
      $handler->display->display_options['style_options'] = array (
        'grouping' => array(),
        'row_class' => '',
        'default_row_class' => 1,
        'row_class_special' => 1,
        'columns' => 2,
        'alignment' => 'horizontal',
        'fill_single_line' => 1,
        'caption' => '',
        'summary' => '',
        );

      /* Field: nid */
      $handler->display->display_options['fields']['nid'] = array( 
        'id' => 'nid',
        'table' => 'node',
        'field' => 'nid',
        'relationship' => 'none',
        'exclude' => 1,
        'group_type' => 'group',
        'alter' => array (
          'alter_text' => 0,
          'make_link' => 0,
          'trim' => 0,
          'word_boundary' => 1,
          'ellipsis' => 1,
          'strip_tags' => 0,
          'html' => 0,
          ),
        'hide_empty' => 0,
        'empty_zero' => 0,
        );

      /* Field: title */
      $handler->display->display_options['fields']['title'] = array (
        'id' => 'title',
        'table' => 'node',
        'field' => 'title',
        'label' => '',
        'relationship' => 'none',
        'exclude' => 0,
        'group_type' => 'group',
        'alter' => array (
          'alter_text' => 0,
          'make_link' => 1,
          'path' => '/node/[nid]',
          'absolute' => 1,
          'trim' => 0,
          'word_boundary' => 1,
          'ellipsis' => 1,
          'strip_tags' => 0,
          'html' => 0,
          ),
        'hide_empty' => 0,
        'empty_zero' => 0,
        'link_to_node' => 0,
        'hide_alter_empty' => 1,
        );

      /* Field: media_url */
      $handler->display->display_options['fields']['media_url'] = array (
        'id' => 'field_media_url',
        'table' => 'field_data_field_media_url',
        'field' => 'field_media_url',
        'label' => '',
        'type' => 'file_rendered',
        'relationship' => 'none',
        'exclude' => 0,
        'group_type' => 'group',
        'alter' => array (
          'alter_text' => 0,
          'make_link' => 0,
          'trim' => 0,
          'word_boundary' => 1,
          'ellipsis' => 1,
          'strip_tags' => 0,
          'html' => 0,
          ),
        'hide_empty' => 0,
        'empty_zero' => 0,
        'link_to_user' => 0,
        'overwrite_anonymous' => 0,
        );

      /* Sort Criteria */
      $handler->display->display_options['sorts']['field_publication_date_value'] = array (
        'id' => 'field_publication_date_value',
        'table' => 'field_data_field_publication_date',
        'field' => 'field_publication_date_value ',
        'order' => 'DESC',
        );

      /* Filters */
      $handler->display->display_options['filters'] = array (
        'type' => array (
          'id' => 'type',
          'table' => 'node',
          'field' => 'type',
          'operator' => 'in',
          'value' => array( 'media' => 'media', ),
          ),
        'field_channel_value' => array (
          'id' => 'field_channel_value',
          'table' => 'field_data_field_channel',
          'field' => 'field_channel_value',
          'relationship' => 'none',
          'group_type' => 'group',
          'ui_name' => '',
          'operator' => '=',
          'value' => $view_id,
          'group' => 1,
          'exposed' => FALSE,
          ),
        'status' => array (
          'id' => 'status',
          'table' => 'node',
          'field' => 'status',
          'relationship' => 'none',
          'group_type' => 'group',
          'ui_name' => '',
          'operator' => '=',
          'value' => 1,
          'group' => 1,
          'exposed' => FALSE,
          ),
        );

      /* Display: Page */
      $handler = $view->new_display('page', 'Page', 'page_1');
      $handler->display->display_options['path'] = 'C' . $view_id;

      $view->save();
      if(Utility::read_setting('log')){ watchdog('Media Auto','Added View ' . $view_id ,NULL, WATCHDOG_DEBUG); }
      return $view;
      }
  }
  
  public static function delete_feed_view($view_name,$view_id){
    $name = $view_name . '_' . ((strlen($view_id) > 10)?substr($view_id, 0, 10):$view_id);
    if ($view = views_get_view($name)) {
      $view->delete();
    }
  }
  
  public static function create_new_content_block(){
    $view =   views_new_view();
    $view->name = 'What\'s New';
    $view_id = $view->name . '_' . strtotime(date('YmdHis'));
    $view->description = 'Recently posted content';
    $view->tag = 'default';
    $view->view_php = '';
    $view->base_table = 'node';
    $view->base_field = 'nid';
    $view->core = '7';
    $view->api_version = '3';
    $view->is_cacheable = FALSE;
    $view->disabled = FALSE;
    
    /* Create the display and populate the options */
    $handler = $view->new_display('default', 'Defaults', 'default');
    $handler->display->display_options['title'] = 'What\'s New';
    $handler->display->display_options['access']['type'] = 'role';
    $handler->display->display_options['access']['role'] = array( '3' => '3', );  
    $handler->display->display_options['cache']['type'] = 'none';
    $handler->display->display_options['exposed_form']['type'] = 'basic';
    $handler->display->display_options['css_class'] = 'media-auto-whats-new';
    $handler->display->display_options['pager'] = array (
      'type' => 'some',
      'options' => array (
        'items_per_page' => '5',
        'offset' => '0',
        'id' => '0',
        ),
      );
    
    /* Handler */
    $handler->display->display_options['style_plugin'] = 'default';
    $handler->display->display_options['style_options'] = array (
      'grouping' => array(),
      'row_class' => '',
      'default_row_class' => 1,
      'row_class_special' => 0,
      );
    
    /* Field: title */
    $handler->display->display_options['fields']['title'] = array (
      'id' => 'title',
      'table' => 'node',
      'field' => 'title',
      'label' => '',
      'relationship' => 'none',
      'exclude' => 0,
      'group_type' => 'group',
      'alter' => array (
        'alter_text' => 0,
        'make_link' => 1,
        'path' => '/node/[nid]',
        'absolute' => 1,
        'trim' => 0,
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
        ),
      'hide_empty' => 0,
      'empty_zero' => 0,
      'link_to_node' => 1,
      'hide_alter_empty' => 1,
      );

    /* Filters */
    $handler->display->display_options['filters'] = array (
      'status' => array (
        'id' => 'status',
        'table' => 'node',
        'field' => 'status',
        'expose' => array ('operator' => FALSE, ),
        'value' => 1,
        'group' => 1,
        'exposed' => FALSE,
        ),
      'type' => array (
        'id' => 'type',
        'table' => 'node',
        'field' => 'type',
        'value' => array( 'media' => 'media', ),
        'exposed' => FALSE,
        ),
      );
    
    /* Sorts */
    $handler->display->display_options['sorts'] = array (
      'field_publication_date_value' => array (
        'id' => 'field_publication_date_value',
        'table' => 'field_data_field_publication_date',
        'field' => 'field_publication_date_value',
        'relationship' => 'none',
        'group_type' => 'group',
        'ui_name' => '',
        'order' => 'DESC',
        'exposed' => FALSE,
        'expose' => array('label'=>''),
      ),
    );
    
    /* Display: Page */
    $handler = $view->new_display('block', 'Block', 'block_1');
    $handler->display->display_options['path'] = $view_id;

    $view->save();
    if(Utility::read_setting('log')){ watchdog('Media Auto','Added View ' . $view_id ,NULL, WATCHDOG_DEBUG); }
    return $view;
    
  }
 
  public static function create_recent_channels_block(){
    $view = views_new_view();
    
    return $view;
  }
  
   public static function create_in_the_news_block(){
    $view = views_new_view();
    $view->name = 'In the News';
    $view_id = $view->name . '_' . strtotime(date('YmdHis'));
    $view->description = 'Content that has been tagged as "In the News"';
    $view->tag = 'default';
    $view->view_php = '';
    $view->base_table = 'node';
    $view->base_field = 'nid';
    $view->core = '7';
    $view->api_version = '3';
    $view->is_cacheable = FALSE;
    $view->disabled = FALSE;
    
    /* Create the display and populate the options */
    $handler = $view->new_display('default', 'Defaults', 'default');
    $handler->display->display_options['title'] = 'In the News';
    $handler->display->display_options['access']['type'] = 'perm';
    $handler->display->display_options['access']['perm'] = 'access_content';  
    $handler->display->display_options['cache']['type'] = 'none';
    $handler->display->display_options['exposed_form']['type'] = 'basic';
    $handler->display->display_options['css_class'] = 'media-auto-in_the_news';
    $handler->display->display_options['pager'] = array (
      'type' => 'some',
      'options' => array (
        'items_per_page' => '5',
        'offset' => '0',
        'id' => '0',
        ),
      );
    
    /* Handler */
    $handler->display->display_options['style_plugin'] = 'default';
    $handler->display->display_options['style_options'] = array (
      'grouping' => array(),
      'row_class' => '',
      'default_row_class' => 1,
      'row_class_special' => 0,
      );
    
    /* Field: title */
    $handler->display->display_options['fields']['title'] = array (
      'id' => 'title',
      'table' => 'node',
      'field' => 'title',
      'label' => '',
      'relationship' => 'none',
      'exclude' => 0,
      'group_type' => 'group',
      'alter' => array (
        'alter_text' => 0,
        'make_link' => 0,
        'path' => '/node/[nid]',
        'absolute' => 1,
        'trim' => 0,
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
        ),
      'hide_empty' => 0,
      'empty_zero' => 0,
      'link_to_node' => 1,
      'hide_alter_empty' => 1,
      );

    /* Filters */
    $handler->display->display_options['filters'] = array (
      'status' => array (
        'id' => 'status',
        'table' => 'node',
        'field' => 'status',
        'expose' => array ('operator' => FALSE, ),
        'value' => 1,
        'group' => 1,
        'exposed' => FALSE,
        ),
      'type' => array (
        'id' => 'type',
        'table' => 'node',
        'field' => 'type',
        'value' => array( 'media' => 'media', ),
        'exposed' => FALSE,
        ),
      'tid' => array(
        'id' => 'tid',
        'table' => 'taxonomy_index',
        'field' => 'tid',
        'group_type' => 'group',
        'ui_name' => '',
        'operator' => 'or',
        'value' => array('0'=>'2',),
        'group' => '1',
        'type' => 'textfield',
        'limit' => 'true',
        'vocabulary' => 'media_auto_tags',
        'hierarchy' => '0',
        
        ),
      );
    
    /* Sorts */
    $handler->display->display_options['sorts'] = array (
      'field_publication_date_value' => array (
        'id' => 'field_publication_date_value',
        'table' => 'field_data_field_publication_date',
        'field' => 'field_publication_date_value',
        'relationship' => 'none',
        'group_type' => 'group',
        'ui_name' => '',
        'order' => 'DESC',
        'exposed' => FALSE,
        'expose' => array('label'=>''),
      ),
    );
    
    /* Display: Page */
    $handler = $view->new_display('block', 'Block', 'block_1');
    $handler->display->display_options['path'] = $view_id;

    $view->save();
    if(Utility::read_setting('log')){ watchdog('Media Auto','Added View ' . $view_id ,NULL, WATCHDOG_DEBUG); }
    return $view;
  }
  
  public static function create_featured_content_block(){
    $view = views_new_view();
    $view->name = 'Featured Content';
    $view_id = $view->name . '_' . strtotime(date('YmdHis'));
    $view->description = 'Content tagged as featured';
    $view->tag = 'default';
    $view->view_php = '';
    $view->base_table = 'node';
    $view->base_field = 'nid';
    $view->core = '7';
    $view->api_version = '3';
    $view->is_cacheable = FALSE;
    $view->disabled = FALSE;
    
    /* Create the display and populate the options */
    $handler = $view->new_display('default', 'Defaults', 'default');
    $handler->display->display_options['title'] = 'Featured Content';
    $handler->display->display_options['access']['type'] = 'perm';
    $handler->display->display_options['access']['perm'] = 'access_content';  
    $handler->display->display_options['cache']['type'] = 'none';
    $handler->display->display_options['exposed_form']['type'] = 'basic';
    $handler->display->display_options['css_class'] = 'media-auto-featured_content';
    $handler->display->display_options['pager'] = array (
      'type' => 'some',
      'options' => array (
        'items_per_page' => '5',
        'offset' => '0',
        'id' => '0',
        ),
      );
    
    /* Handler */
    $handler->display->display_options['style_plugin'] = 'default';
    $handler->display->display_options['style_options'] = array (
      'grouping' => array(),
      'row_class' => '',
      'default_row_class' => 1,
      'row_class_special' => 0,
      );
    
    /* Field: title */
    $handler->display->display_options['fields']['title'] = array (
      'id' => 'title',
      'table' => 'node',
      'field' => 'title',
      'label' => '',
      'relationship' => 'none',
      'exclude' => 0,
      'group_type' => 'group',
      'alter' => array (
        'alter_text' => 0,
        'make_link' => 0,
        'path' => '/node/[nid]',
        'absolute' => 1,
        'trim' => 0,
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
        ),
      'hide_empty' => 0,
      'empty_zero' => 0,
      'link_to_node' => 1,
      'hide_alter_empty' => 1,
      );

    /* Filters */
    $handler->display->display_options['filters'] = array (
      'status' => array (
        'id' => 'status',
        'table' => 'node',
        'field' => 'status',
        'expose' => array ('operator' => FALSE, ),
        'value' => 1,
        'group' => 1,
        'exposed' => FALSE,
        ),
      'type' => array (
        'id' => 'type',
        'table' => 'node',
        'field' => 'type',
        'value' => array( 'media' => 'media', ),
        'exposed' => FALSE,
        ),
      'tid' => array(
        'id' => 'tid',
        'table' => 'taxonomy_index',
        'field' => 'tid',
        'group_type' => 'group',
        'ui_name' => '',
        'operator' => 'or',
        'value' => array('0'=>'1',),
        'group' => '1',
        'type' => 'textfield',
        'limit' => 'true',
        'vocabulary' => 'media_auto_tags',
        'hierarchy' => '0',
        
        ),
      );
    
    /* Sorts */
    $handler->display->display_options['sorts'] = array (
      'field_publication_date_value' => array (
        'id' => 'field_publication_date_value',
        'table' => 'field_data_field_publication_date',
        'field' => 'field_publication_date_value',
        'relationship' => 'none',
        'group_type' => 'group',
        'ui_name' => '',
        'order' => 'DESC',
        'exposed' => FALSE,
        'expose' => array('label'=>''),
      ),
    );
    
    /* Display: Page */
    $handler = $view->new_display('block', 'Block', 'block_1');
    $handler->display->display_options['path'] = $view_id;

    $view->save();
    if(Utility::read_setting('log')){ watchdog('Media Auto','Added View ' . $view_id ,NULL, WATCHDOG_DEBUG); }
    return $view;
    
  }
  
  public static function delete_view($name){
    if ($view = views_get_view($name)) {
      $view->delete();
    }
  }

}