<?php
/** The Collection object
*/
/**	
 * @file Collection.php
 * @name Collection 		  
 * @author Susanne Coates
 * @copyright (c)2015 University of Maryland
 * @todo
 */
/*
 This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License version 2
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

require_once 'Account.php';
require_once 'Views.php';
require_once 'vimeo/autoload.php';

interface CollectionInterface {
  public function setID($id);
  public function getID();
  //CRUD functions
  public function create();
  public function retrieve();
  public function update();
  public function delete();
  // Object validation
  public function validate();
}

class Collection implements CollectionInterface {
  private $collection_id;
  public $account; // the account to which this channel is linked
  public $etag;
  public $title;
  public $description;
  public $type;
  public $publication_date; 
  public $thumb_default;
  public $thumb_high;
  public $thumb_low;
  public $enabled;
  public $parent;
      
  // Class Initialzer
  public function Collection() {
    $this->collection_id = -1;
    $this->account = new Account();
    $this->etag = '';
    $this->title = '';
    $this->description = '';
    $this->type = '';
    $this->publication_date = ''; 
    $this->thumb_default = '';
    $this->thumb_high = '';
    $this->thumb_low = '';
    $this->enabled = false;
    $this->parent = '';
  }

  public function setID($id){
    $this->collection_id = $id;
  }
  
  public function getID() {
    return $this->collection_id;
  }

  //Stores a new channel object to the database
  public function create() {
    $fields_array = array (
      'collection_id'  => $this->collection_id,
      'account_id' => $this->account->getID(),
      'etag' => $this->etag,
      'title'  => $this->title,
      'description' => $this->description,
      'type' => $this->type,
      'publication_date' => $this->publication_date,
      'thumb_default' => $this->thumb_default,
      'thumb_high' => $this->thumb_high,
      'thumb_low'     => $this->thumb_low,
      'enabled'     => $this->enabled,
      'parent'     => $this->parent,
    ); 
    Utility::insert_row('media_auto_collections', $fields_array);
    if(Utility::read_setting('log')){
      $messagetemp = 'Added Collection: ' . $this->collection_id . ', Title: ' . $this->title;
      watchdog('Media Auto', $messagetemp, NULL, WATCHDOG_DEBUG);
    }
  }
  
  // returns a fully loaded channel object
  public function retrieve() {

    $query = db_select('media_auto_collections', 'n')
	    ->fields('n', array('collection_id','account_id','etag','title','description','type','publication_date','thumb_default','thumb_high','thumb_low','enabled','parent'))
		  ->condition('collection_id',$this->collection_id,'=')
	    ->execute();
		$result = $query->fetch();
    if(!empty($result->collection_id)) {
      $this->account->setID($result->account_id);
      $this->account->retrieve();
      $this->etag = $result->etag;
      $this->title = $result->title;
      $this->description = $result->description;
      $this->type = $result->type;
      $this->publication_date = $result->publication_date;
      $this->thumb_default = $result->thumb_default;
      $this->thumb_high = $result->thumb_high;
      $this->thumb_low = $result->thumb_low;
      $this->enabled = $result->enabled;
      $this->parent = $result->parent;
      return true;
    } else {
      return false;
    }
  }
  
  //Update database fields for an existing channel
  public function update() {
    // get the existing values from the table
    $query = db_select('media_auto_collections', 'n')
	    ->fields('n', array('collection_id','account_id','etag','title','description','type','publication_date','thumb_default','thumb_high','thumb_low','enabled','parent'))
		  ->condition('collection_id',$this->collection_id,'=')
	    ->execute();
		$result = $query->fetch();
    // compare the class values with the existing values and update the table columns with changed values
    ($result->account_id <> $this->account->getID())?   Utility::update_cell('media_auto_collections', 'collection_id', $this->collection_id, 'account_id', $this->account->account_id):'';
    ($result->etag <> $this->etag)?               Utility::update_cell('media_auto_collections', 'collection_id', $this->collection_id, 'etag', $this->etag):'';
    ($result->title <> $this->title)?             Utility::update_cell('media_auto_collections', 'collection_id', $this->collection_id, 'title', $this->title):'';
    ($result->description <> $this->description)? Utility::update_cell('media_auto_collections', 'collection_id', $this->collection_id, 'description', $this->description):'';
    ($result->type <> $this->type)?               Utility::update_cell('media_auto_collections', 'collection_id', $this->collection_id, 'type', $this->type):'';
    ($result->publication_date <> $this->publication_date)? Utility::update_cell('media_auto_collections', 'collection_id', $this->collection_id, 'publication_date', $this->publication_date):'';
    ($result->thumb_default <> $this->thumb_default)? Utility::update_cell('media_auto_collections', 'collection_id', $this->collection_id, 'thumb_default', $this->thumb_default):'';
    ($result->thumb_high <> $this->thumb_high)? Utility::update_cell('media_auto_collections', 'collection_id', $this->collection_id, 'thumb_high', $this->thumb_high):'';
    ($result->thumb_low <> $this->thumb_low)?   Utility::update_cell('media_auto_collections', 'collection_id', $this->collection_id, 'thumb_low', $this->thumb_low):'';
    ($result->enabled <> $this->enabled)?       Utility::update_cell('media_auto_collections', 'collection_id', $this->collection_id, 'enabled', $this->enabled):'';
    ($result->parent <> $this->parent)?       Utility::update_cell('media_auto_collections', 'collection_id', $this->collection_id, 'parent', $this->parent):'';
    if(Utility::read_setting('log')){
     $messagetemp = 'Updated Collection: ' . $this->collection_id . ', Title: ' . $this->title;
     watchdog('Media Auto', $messagetemp, NULL, WATCHDOG_DEBUG);
    }
  }
  
  public function delete() {
    Views::delete_feed_view($this->title,$this->getID());
    // Delete the view
    //$view = views_ui_cache_load($name);
    //dpm($view);
    //$view->delete();
    //views_object_cache_clear('view', $view->name);
    
    Utility::drop_row('media_auto_collections','collection_id',$this->getID());
    
    if(Utility::read_setting('log')){
      $messagetemp = 'Deleted Collection: ' . $this->getID() . ', Title: ' . $this->title;
      watchdog('Media Auto', $messagetemp, NULL, WATCHDOG_DEBUG);
    }
  }
  
  /** Validate the Collection Object
   *  Check that the resources pointed to by the relevent fields in the object are valid (do not return a 404, etc.)
   */
  public function validate() {

  }
}