<?php
/** The Account functions
*/
/**		
 * @file Account.php
 * @name Account   
 * @author Susanne Coates
 * @copyright (c)2015 University of Maryland
 * @todo
 */
/*
 This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License version 2
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

require_once 'Provider.php';
require_once 'Utility.php';

interface AccountInterface {
  public function setID($id);
  public function getID();
  //CRUD functions
  public function create();
  public function retrieve();
  public function update();
  public function delete(); 
  // Object validation
  public function validate();
  
  public function updateVimeoAccountDetails();
  public function updateYoutubeAccountDetails();
}
class Account implements AccountInterface{
  private $account_id;
  public $provider;
  public $title;
  public $description;
  public $search_path;
  public $query;
  public $thumb_default;
  public $thumb_high;
  public $thumb_low;
  
  // Class Initialzer
  public function Account() {
    $this->account_id = -1;
    $this->provider = new Provider();
    $this->title = '';
    $this->description = '';
    $this->search_path = '';
    $this->query = '';
    $this->thumb_default = '';
    $this->thumb_high = '';
    $this->thumb_low = '';
  }

  public function setID($id){
    $this->account_id = $id;
  }
  
  public function getID() {
    return $this->account_id;
  }
  
  // returns a fully loaded AccountObject for the given account ID
  public function retrieve() {
    $query = db_select('media_auto_accounts', 'n')
	    ->fields('n', array('account_id','provider_id','title','description','search_path','query','thumb_default','thumb_high','thumb_low'))
		  ->condition('account_id',$this->account_id,'=')
	    ->execute();
		$result = $query->fetch();
    
    if(!empty($result->account_id)) {
      // Get the provider information for the account
      $this->provider->setID($result->provider_id);
      $this->provider->retrieve();    
      $this->title = $result->title;
      $this->description = $result->description;
      $this->search_path = $result->search_path;
      $this->query = $result->query;
      $this->thumb_default = $result->thumb_default;
      $this->thumb_high = $result->thumb_high;
      $this->thumb_low = $result->thumb_low;
      return true;
    } else {
      return false;
    }
  }
  
  //Stores a new account object to the database
  public function create() {
    $fields_array = array (
      'account_id'  => $this->account_id,
      'provider_id' => $this->provider->getID(),
      'title'  => $this->title,
      'description' => $this->description,
      'search_path' => $this->search_path,
      'query' => $this->query,
      'thumb_default' => $this->thumb_default,
      'thumb_high' => $this->thumb_high,
      'thumb_low'     => $this->thumb_low,
    ); 
    Utility::insert_row('media_auto_accounts', $fields_array);
    if(Utility::read_setting('log')){
      $messagetemp = 'Added Account: ' . $this->account_id . ', Title: ' . $this->title;
      watchdog('Media Auto', $messagetemp, NULL, WATCHDOG_DEBUG);
    }
  }
  
  //Update the fields in an existing account object
  public function update() {
    // get the existing values from the table
    $query = db_select('media_auto_accounts', 'n')
	    ->fields('n', array('account_id','provider_id','title','description','search_path','query','thumb_default','thumb_high','thumb_low'))
		  ->condition('account_id',$this->account_id,'=')
	    ->execute();
		$result = $query->fetch();
    // compare the class values with the existing values and update the table columns with changed values
    ($result->title <> $this->title)?                 Utility::update_cell('media_auto_accounts', 'account_id', $this->account_id, 'title',$this->title):'';
    ($result->description <> $this->description)?     Utility::update_cell('media_auto_accounts', 'account_id', $this->account_id, 'description',$this->description):'';
    ($result->search_path <> $this->search_path)?     Utility::update_cell('media_auto_accounts', 'account_id', $this->account_id, 'search_path',$this->search_path):'';
    ($result->query <> $this->query)?                 Utility::update_cell('media_auto_accounts', 'account_id', $this->account_id, 'query',$this->query):'';
    ($result->thumb_default <> $this->thumb_default)? Utility::update_cell('media_auto_accounts', 'account_id', $this->account_id, 'thumb_default',$this->thumb_default):'';
    ($result->thumb_high <> $this->thumb_high)?       Utility::update_cell('media_auto_accounts', 'account_id', $this->account_id, 'thumb_high',$this->thumb_high):'';
    ($result->thumb_low <> $this->thumb_low)?         Utility::update_cell('media_auto_accounts', 'account_id', $this->account_id, 'thumb_low',$this->thumb_low):'';
    
    if(Utility::read_setting('log')){
      $messagetemp = 'Updated Account: ' . $this->account_id . ', Title: ' . $this->title;
      watchdog('Media Auto', $messagetemp, NULL, WATCHDOG_DEBUG);
    }
  }
  
  public function delete() {
    Utility::drop_row('media_auto_accounts','account_id',$this->getID());
    if(Utility::read_setting('log')){
      $messagetemp = 'Deleted Account: ' . $this->account_id . ', Title: ' . $this->title;
      watchdog('Media Auto', $messagetemp, NULL, WATCHDOG_DEBUG);
    }
  }
  
  /** Validate the Account Object
   *  Check that the resources pointed to by the relevent fields in the object are valid (do not return a 404, etc.)
   */
  public function validate() {
    // check provider_id points to a valid provider
    // check $this->thumb_default, $this->thumb_high, $this->thumb_low are valid
    
  }
  
   /** Get the details for the vimeo account.
   * 
   * @param string $account_id
   * @return an array containing the account details on success or false on failure.
   */
  public function updateVimeoAccountDetails() {
    switch($this->provider->auth_method) {
      case 'oauth2':
        $lib = new \Vimeo\Vimeo($this->provider->auth_field_1, $this->provider->auth_field_2);
        $lib->setToken($this->provider->auth_field_3);
        $url = '/users/' . $this->getID();
        $params = array();
        $details = $lib->request($url, $params);
        $this->title = $details['body']['name'];
        $this->description = $details['body']['bio'];
        $this->thumb_default = $details['body']['pictures']['sizes']['2']['link'];
        $this->thumb_high = $details['body']['pictures']['sizes']['3']['link'];
        $this->thumb_low = $details['body']['pictures']['sizes']['1']['link'];
        $this->update();

        return true;
    }
    return false;
  }
  
      /** Get the details for a youtube account
   * 
   * @param type $account_id
   * @return array or boolean
   */
  public function updateYoutubeAccountDetails() {
    switch($this->provider->auth_method) {
      case 'oauth2':

      break;

      case 'apikey':
        $url = $this->provider->api_access_url . '/youtube/v3/channels';
        $data = array (
          'part'=> 'snippet',
          'forUsername' => $this->getID(),
          'key' => $this->provider->auth_field_1, 
        );
        $full_url = url($url, array('query' => $data));
        $details = drupal_json_decode(drupal_http_request($full_url)->data)['items'][0];
        $this->title = $details['snippet']['title'];
        $this->description = $details['snippet']['description'];
        $this->thumb_default = $details['snippet']['thumbnails']['default']['url'];
        $this->thumb_high = $details['snippet']['thumbnails']['high']['url'];
        $this->thumb_low = $details['snippet']['thumbnails']['medium']['url'];
        $this->update();
        return true;
    }
    return false;
  }
}