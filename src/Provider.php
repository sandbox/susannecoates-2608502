<?php
/** Provider Functions
*/

/**				  
 * @file Provider.php
 * @name Provider 
 * @author Susanne Coates
 * @copyright (c)2015 University of Maryland
 * @todo
 */
/*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License version 2
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

require_once 'Utility.php';

interface ProviderInterface {
  public function setID($id);
  public function getID();
  //CRUD functions
  public function create();
  public function retrieve();
  public function update();
  public function delete();  
  // Object validation
  public function validate();
}

class Provider implements ProviderInterface {
  private $provider_id;
  public $auth_method;
  public $auth_field_1;
  public $auth_field_2;
  public $auth_field_3;
  public $auth_field_4;
  public $api_access_url;
  public $watch_url;
  public $branding_url;
      
  // Initialize
  public function Provider(){
    $this->provider_id = '';
    $this->auth_method = 'none';
    $this->auth_field_1 = '';
    $this->auth_field_2 = '';
    $this->auth_field_3 = '';
    $this->auth_field_4 = '';
    $this->api_access_url = '';
    $this->watch_url = '';
    $this->branding_url = '';
  }

  public function setID($id) {
    $this->provider_id = $id;
  }
  
  public function getID() {
    return $this->provider_id;
  }
  
  //Stores a new channel object to the database
  public function create() {
    $fields_array = array (
      'provider_id'  => $this->provider_id,
      'auth_method'  => $this->auth_method,
      'auth_field_1' => $this->auth_field_1,
      'auth_field_2' => $this->auth_field_2,
      'auth_field_3' => $this->auth_field_3,
      'auth_field_4' => $this->auth_field_4,
      'api_access_url' => $this->api_access_url,
      'watch_url'    => $this->watch_url,
      'branding_url' => $this->branding_url,
    ); 
    Utility::insert_row('media_auto_providers', $fields_array);
  }
  
  // returns a fully loaded provider object
  public function retrieve() {
    $query = db_select('media_auto_providers', 'n')
      ->fields('n', array('provider_id','auth_method','auth_field_1','auth_field_2','auth_field_3','auth_field_4','api_access_url','watch_url','branding_url'))
      ->condition('provider_id',$this->provider_id,'=')
	    ->execute();
		$result = $query->fetch();
    if(!empty($result->provider_id)) {
      $this->provider_id  = $result->provider_id;
      $this->auth_method  = $result->auth_method;
      $this->auth_field_1 = $result->auth_field_1;
      $this->auth_field_2 = $result->auth_field_2;
      $this->auth_field_3 = $result->auth_field_3;
      $this->auth_field_4 = $result->auth_field_4;
      $this->api_access_url = $result->api_access_url;
      $this->watch_url    = $result->watch_url;
      $this->branding_url = $result->branding_url;
      return true;
    } else {
      return false;
    } 
  }
  
  //Update database fields for an existing channel
  public function update() {
    // get the existing values from the table
    $query = db_select('media_auto_providers', 'n')
	    ->fields('n', array('provider_id','auth_method','auth_field_1','auth_field_2','auth_field_3','auth_field_4','api_access_url','watch_url', 'branding_url'))
		  ->condition('provider_id',$this->provider_id,'=')
	    ->execute();
		$result = $query->fetch();
    // compare the class values with the existing values and update the table columns with changed values
    ($result->auth_method <> $this->auth_method)?    Utility::update_cell('media_auto_providers', 'provider_id', $this->provider_id, 'auth_method',$this->auth_method):'';
    ($result->auth_field_1 <> $this->auth_field_1)?  Utility::update_cell('media_auto_providers', 'provider_id', $this->provider_id, 'auth_field_1',$this->auth_field_1):'';
    ($result->auth_field_2 <> $this->auth_field_2)?  Utility::update_cell('media_auto_providers', 'provider_id', $this->provider_id, 'auth_field_2',$this->auth_field_2):'';
    ($result->auth_field_3 <> $this->auth_field_3)?  Utility::update_cell('media_auto_providers', 'provider_id', $this->provider_id, 'auth_field_3',$this->auth_field_3):'';
    ($result->auth_field_4 <> $this->auth_field_4)?  Utility::update_cell('media_auto_providers', 'provider_id', $this->provider_id, 'auth_field_4',$this->auth_field_4):'';
    ($result->api_access_url <> $this->api_access_url)? Utility::update_cell('media_auto_providers', 'provider_id', $this->provider_id, 'api_access_url',$this->api_access_url):'';
    ($result->watch_url <> $this->watch_url)?        Utility::update_cell('media_auto_providers', 'provider_id', $this->provider_id, 'watch_url',$this->watch_url):'';
    ($result->branding_url <> $this->branding_url)?  Utility::update_cell('media_auto_providers', 'provider_id', $this->provider_id, 'branding_url',$this->branding_url):'';
  }
  
  public function delete() {
    Utility::drop_row('media_auto_providers', 'provider_id', $this->provider_id);
  }
  
  /** Validate the Provider Object
   *  Check that the resources pointed to by the relevent fields in the object are valid (do not return a 404, etc.)
   */
  public function validate() {
    // check $this->api_access_url and $this->branding_url
    $host = strstr($this->api_access_url,':');
    $len = strlen($host);
    $host = substr($host,1,$len-1);
    Utility::ping($host, $timeout = 1);
  }
  
}