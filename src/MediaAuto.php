<?php
/**		
 * @file MediaAuto.php
 * @name MediaAuto   
 * @author Susanne Coates
 * @copyright (c)2015 University of Maryland
 * @todo
 */
/*
 This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License version 2
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

require_once 'Provider.php';
require_once 'Account.php';
require_once 'Collection.php';
require_once 'Media.php';
require_once 'MediaEnumeration.php';
require_once 'Utility.php';
require_once 'Views.php';

interface MediaAutoInterface {
  public static function updateAccounts();
  public static function updateCollections();
  public static function updateMedia();
  public static function deleteAccountRecursive($account_id);
  public static function deleteCollectionRecursive($channel_id);
}

final class MediaAuto implements MediaAutoInterface {

 // Update the account fields in the database with the third party provider information
 public static function updateAccounts() {
   $accountListItems = MediaEnumeration::retrieveAllAccounts();
   foreach($accountListItems as $accountListItem) {
      $account = new Account();
      $account->setID($accountListItem->getID());
      $account->retrieve();
      switch($account->provider->getID()) {
        case 'vimeo':
          $account->updateVimeoAccountDetails();
          break;
        case 'youtube':
          $account->updateYoutubeAccountDetails();
          break;
      }
      unset($account);
   }
   if(Utility::read_setting('log')){ watchdog('Media Auto','Account Update Completed', NULL, WATCHDOG_DEBUG); }
  }
  
  public static function updateCollections() {
    $accountListItems = MediaEnumeration::retrieveAllAccounts();
    foreach($accountListItems as $accountListItem) {
      $account = new Account();
      $account->setID($accountListItem->getID());
      $account->retrieve();
      
      switch($account->provider->getID()) {
        case 'vimeo':
          MediaEnumeration::updateVimeoCollections($account);
          break;
        case 'youtube':
          MediaEnumeration::updateYoutubeCollections($account);
          break;
        
      }
      unset($account); 
   }
   if(Utility::read_setting('log')){watchdog('Media Auto','Collection Update Completed',NULL, WATCHDOG_DEBUG); }
  }
  
    public static function validateCollections(){
    
  }
  

  
  /** Index through all of the channels for all of the accounts and update the media.
   *  This function both adds new media found on the channels and updates any fields 
   *  for media already in the DB that has changed since the last time this function was called.
   */
  public static function updateMedia() {
    // index through the accounts and get the content
    $accounts = MediaEnumeration::retrieveAllAccounts();
    foreach($accounts as $account){
      //dpm($account);
      switch($account->provider->getID()){
        
        case 'vimeo':
          $collections = MediaEnumeration::retrieveAllCollections($account);
          foreach($collections as $collection){
            MediaEnumeration::updateVimeoCollectionMedia($collection);
          }
        break;
        
        case 'youtube':
          $collections = MediaEnumeration::retrieveAllCollections($account);
          foreach($collections as $collection){
            MediaEnumeration::updateYoutubeCollectionMedia($collection);
          }
        break;
        
      } //switch
      
    }
    if(Utility::read_setting('log')){ watchdog('Media Auto','Media Update Completed',NULL, WATCHDOG_DEBUG); }
  }
  
  public static function validateMedia(){
    
  }
  
  /* Delete an account and all of the associated content
   * 
   */
  public static function deleteAccountRecursive($account_id){
    
    return true;
  }
  
  /* Delete a channel and all of the associated content
   * 
   */
  public static function deleteCollectionRecursive($collection_id){
    
    return true;
  }
}
