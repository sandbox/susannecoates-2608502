<?php
/** The functions in this class get and operate on lists (Arrays, objects, etc.)
 *  via either ReSTful interfaces or database interaction
 */

/**		
 * @file MediaEnumeration.php
 * @name MediaEnumeration  
 * @author Susanne Coates
 * @copyright (c)2015 University of Maryland
 * @todo
 */
/*
 This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License version 2
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

require_once 'Utility.php';
require_once 'vimeo/autoload.php';

/** The MediaEnumeration class returns lists of ID's */
interface MediaEnumerationInterface {
  //Media DB operations
  public static function checkProvider($provider_id);
  public static function retrieveAllProviders();
  public static function checkAccount($account_id);
  public static function retrieveAllAccounts();
  public static function checkCollection($collection_id);
  public static function isCollectionEnabled($collection_id);
  public static function checkMedia($media_id);
  public static function retrieveAllCollections($account_id);
  public static function retrieveAllMedia($collection_id);
  public static function changeCollectionMediaStatus($collection, $status);
  public static function recursiveDeleteAccount($account);
  // Vimeo ReST functions (mostly)
  public static function getVimeoMain($account);
  public static function getVimeoPortfolios($account);
  public static function getVimeoAlbums($account);
  public static function updateVimeoCollections($account);
  public static function getVimeoMediaRecursive($collection, $recurse, $max_depth, $response);
  public static function getVimeoCollectionMedia($collection);
  public static function updateVimeoCollectionMedia($collection);
  // Youtube ReST Functions (Mostly)
  public static function getYoutubePlaylists($account, $collection_id);
  public static function getYoutubeChannels($account);
  public static function updateYoutubeCollections($account);
  public static function getYoutubeCollectionMediaRecursive($collection, $recurse, $max_depth, $response);
  public static function getYoutubeMediaDetails($media_id);
  public static function getYoutubeCollectionMedia($collection);
  public static function updateYoutubeCollectionMedia($collection);
  // Utility Functions
  public static function parseMediaIDFromSearchResults($searchResultItem, $collection_type);
}

class MediaEnumeration implements MediaEnumerationInterface {

  
  /* DB INTERACTION FUNCTIONS                                                           
   * Functions that interact with the database to get lists of things                   
   * *********************************************************************************
  */
  
  /** Check if a provider is already in the providers table
   * 
   * @param type $provider_id
   * @return type
   */
  public static function checkProvider($provider_id){
    $query = db_select('media_auto_providers', 'n')
	    ->fields('n', array('provider_id'))
		  ->condition('provider_id',$provider_id,'=')
	    ->execute();
		$result = $query->fetch();
    return (!empty($result->provider_id));
  }
  
  /** Return an array containing all of the providers
   * 
   * @return type
   */
  public static function retrieveAllProviders() {
    $query = db_select('media_auto_providers', 'n')
      ->fields('n', array('provider_id'));
		$providerList = $query->execute();
    $providers = array();
    $i=0;
    foreach($providerList as $providerListItem) {
      $providers[$i] = new Provider();
      $providers[$i]->setID($providerListItem->provider_id);
      $providers[$i]->retrieve();
      $i++;
    }
    return $providers;
  }
  
  /** Check if an account is already in the accounts table
   * 
   * @param type $account_id
   * @return type
   */
  public static function checkaccount($account_id){
    $query = db_select('media_auto_accounts', 'n')
	    ->fields('n', array('account_id'))
		  ->condition('account_id',$account_id,'=')
	    ->execute();
		$result = $query->fetch();
    return (!empty($result->account_id));
  }
  
  /** Return a list of all the accounts  
   * 
   * @return type
   */
  public static function retrieveAllAccounts() {
    $query = db_select('media_auto_accounts', 'n');
    $query->fields('n', array('account_id'));
    $accountList = $query->execute();
    $i=0;
    $accounts = array();
    foreach($accountList as $accountListItem){
      $accounts[$i] = new Account();
      $accounts[$i]->setID($accountListItem->account_id);
      $accounts[$i]->retrieve();
      $i++;
    }
		return $accounts;
  }
  
  /** Check if a channel is already in the channels table
   * 
   * @param type $collection_id
   * @return type
   */
  public static function checkCollection($collection_id){
    $query = db_select('media_auto_collections', 'n')
	    ->fields('n', array('collection_id'))
		  ->condition('collection_id',$collection_id,'=')
	    ->execute();
		$result = $query->fetch();
    return (!empty($result->collection_id));
  }
  
  /** Check if a channel is already in the channels table
   * 
   * @param type $collection_id
   * @return type
   */
  public static function isCollectionEnabled($collection_id){
    $query = db_select('media_auto_collections', 'n')
	    ->fields('n', array('collection_id','enabled'))
		  ->condition('collection_id',$collection_id,'=')
	    ->execute();
		$enabled = $query->fetch()->enabled;
    return ($enabled);
  }
  
  /** Check if a media is already in the channels table
   * 
   * @param type $collection_id
   * @return type
   */  
  public static function checkMedia($media_id){
    $query = db_select('media_auto_media_index', 'n')
	    ->fields('n', array('media_id'))
		  ->condition('media_id',$media_id,'=')
	    ->execute();
		$result = $query->fetch();
    return (!empty($result->media_id));
  }
  
  
  /** Return a list of all the collections for the given account.
   * 
   * @param Object $account
   * @return Object[] $collections
   */
  public static function retrieveAllCollections($account) {
    $query = db_select('media_auto_collections', 'n');
    $query->fields('n', array('collection_id','account_id','type','parent'));
    ($account->getID() <> -1)?$query->condition('account_id', $account->getID(),'='):'';
    $query->orderby('account_id','ASC');
    $query->orderby('parent','ASC');
		$collectionList = $query->execute();
    $i=0;
    $collections = array();
    foreach($collectionList as $collectionListItem){
        $collections[$i] = new Collection();
        $collections[$i]->setID($collectionListItem->collection_id);
        $collections[$i]->retrieve();
        $i++;
    }
    return $collections;
  }
  
  /** Retrieve all of the media for the given collection.
   * 
   * @param type $channelID
   * @return type
   */
  public static function retrieveAllMedia($collection = NULL) {
    $query = db_select('media_auto_media_index', 'mami');
    $query->leftjoin('field_revision_field_publication_date','frfpd', 'mami.nid = frfpd.entity_id');
    $query->fields('mami', array('media_id','collection_id','nid'));
    $query->fields('frfpd', array('field_publication_date_value',));
    (!empty($collection))?$query->condition('mami.collection_id', $collection->getID(),'='):'';
    $query->orderby('frfpd.field_publication_date_value','DESC');
    $mediaList = $query->execute();
    $i=0;
    $media = array(); 
    foreach($mediaList as $mediaListItem){
      $media[$i] = new Media();
      $media[$i]->setID($mediaListItem->media_id);
      $media[$i]->retrieve();
      $i++;
    }
		return $media;
  }

    /** Publish/unpublish all of the media in a collection
   *  
   * @param type $collection
   * @param type $status (0 = unpublished, 1=published)
   */
  public static function changeCollectionMediaStatus($collection, $status){
    foreach(MediaEnumeration::retrieveAllMedia($collection) as $media){
      $media->status = $status && !$media->hide;
      $media->update();
    }
  }
  
  public static function recursiveDeleteAccount($account){
    $collections = MediaEnumeration::retrieveAllCollections($account);
    foreach($collections as $collection){
      $mediaList = MediaEnumeration::retrieveAllMedia($collection);
      foreach($mediaList as $media){
         $media->delete(1);
      }
      $collection->delete();
    }
    $account->delete();
  }
  
  public static function parseMediaIDFromSearchResults($searchResultItem, $collection_type){
    dpm($collection_type);
    list($provider,$type) = explode('#', $collection_type);
    switch($provider){
      case 'vimeo':
        switch($type){
          case 'album':
            break;
          case 'portfolio':
            break;
        }
        return false;
      case 'youtube':
        switch($type){
          case 'channel':
            return (!empty($channelMediaResult['id']['videoId']))?$channelMediaResult['id']['videoId']:false;
          case 'playlist':
            return (!empty($searchResultItem['snippet']['resourceId']['videoId']))?$searchResultItem['snippet']['resourceId']['videoId']:false;
        }
       return false;
    }
  }
  
  
  
  /** ReSTful FUNCTIONS 
   * Functions that interact with the third party platform ReST interface to get lists of things
   * ********************************************************************************************
   */
  
  
  /*******************************
   Vimeo
   ******************************* 
   */
  
  /** Get a list of the portfolios available for the given account
   * 
   * @param type $account
   * @return boolean
   */
  
   public static function getVimeoMain($account) {
    switch($account->provider->auth_method) {
      case 'oauth2':
        $lib = new \Vimeo\Vimeo($account->provider->auth_field_1, $account->provider->auth_field_2);
        $lib->setToken($account->provider->auth_field_3);
        $url = '/users/' . $account->getID();
        $params = array('per_page'=> Utility::read_setting('max_results'), 'sort'=>'date','direction'=>'desc',);
        $response = $lib->request($url, $params);
        return $response['body'];
    }
    return false;
  }
  
  public static function getVimeoPortfolios($account) {
    switch($account->provider->auth_method) {
      case 'oauth2':
        $lib = new \Vimeo\Vimeo($account->provider->auth_field_1, $account->provider->auth_field_2);
        $lib->setToken($account->provider->auth_field_3);
        $url = '/users/' . $account->getID() . '/portfolios';
        $params = array('per_page'=> Utility::read_setting('max_results'), 'sort'=>'date','direction'=>'desc',);
        $response = $lib->request($url, $params);
        return $response['body']['data'];
    }
    return false;
  }
  
  public static function getVimeoAlbums($account) {
    switch($account->provider->auth_method) {
      case 'oauth2':
        $lib = new \Vimeo\Vimeo($account->provider->auth_field_1, $account->provider->auth_field_2);
        $lib->setToken($account->provider->auth_field_3);
        $url = '/users/' . $account->getID() . '/albums';
        $params = array('per_page'=> Utility::read_setting('max_results'), 'sort'=>'date','direction'=>'desc',);
        $response = $lib->request($url, $params);
        return $response['body']['data'];
    }
    return false;
  }
  
    /** Update the vimeo collections for the account
   * 
   * @param type $account
   * @return boolean
   */
  public static function updateVimeoCollections($account) {
    switch($account->provider->auth_method) {
      case 'oauth2':
        // On vimeo, all of the uploaded videos are in the top level account.
        $collectionSearchResult = MediaEnumeration::getVimeoMain($account);
        //dpm($collectionSearchResult);
        $collection = new Collection();
        $collection->account = $account;
        $collection->setID($account->getID());
        
        if(!MediaEnumeration::checkCollection($account->getID())) {
          $collection->etag = '';
          $collection->publication_date = date('Y-m-d H:i:s',strtotime($collectionSearchResult['created_time']));
          $collection->title = $collectionSearchResult['name'];
          $collection->description = (!empty($collectionSearchResult['bio']))?$collectionSearchResult['bio']:'No description available';
          $collection->type = 'vimeo#main';
          $collection->thumb_default = (!empty($collectionSearchResult['pictures']['sizes']['1']['link']))?
            $collectionSearchResult['pictures']['sizes']['1']['link']:$account->thumb_default;
          $collection->thumb_high = (!empty($collectionSearchResult['pictures']['sizes']['2']['link']))?
            $collectionSearchResult['pictures']['sizes']['2']['link']:$account->thumb_default;
          $collection->thumb_low = (!empty($collectionSearchResult['pictures']['sizes']['0']['link']))?
              $collectionSearchResult['pictures']['sizes']['0']['link']:$account->thumb_default;
          $collection->enabled = 0;
          $collection->parent = '';
          $collection->create(); 
        } else {
          $collection->retrieve();
          $collection->etag = '';
          $collection->publication_date = date('Y-m-d H:i:s',strtotime($collectionSearchResult['created_time']));
          $collection->title = $collectionSearchResult['name'];
          $collection->description = (!empty($collectionSearchResult['bio']))?$collectionSearchResult['bio']:'No description available';
          $collection->type = 'vimeo#main';
          $collection->thumb_default = (!empty($collectionSearchResult['pictures']['sizes']['1']['link']))?
            $collectionSearchResult['pictures']['sizes']['1']['link']:$account->thumb_default;
          $collection->thumb_high = (!empty($collectionSearchResult['pictures']['sizes']['2']['link']))?
            $collectionSearchResult['pictures']['sizes']['2']['link']:$account->thumb_default;
          $collection->thumb_low = (!empty($collectionSearchResult['pictures']['sizes']['0']['link']))?
              $collectionSearchResult['pictures']['sizes']['0']['link']:$account->thumb_default;
          $collection->parent = '';
          $collection->update();
        }
        unset($collectionSearchResult);
        unset($collection);
        
        // The albums are organized under the account and contain the videos grouped as the user has defined.
        $collectionSearchResults = MediaEnumeration::getVimeoAlbums($account);
        foreach($collectionSearchResults as $collectionSearchResult){
          $collectionID = (preg_match('/\/users\/(\d+)\/albums\/(\d+)/',$collectionSearchResult['uri'], $matches))? $matches[2]:$collectionSearchResult['name'];

          $collection = new Collection();
          $collection->account = $account;
          $collection->setID($collectionID);

          
          if(!MediaEnumeration::checkCollection($collectionID)) {          
            $collection->etag = '';
            $collection->publication_date = date('Y-m-d H:i:s',strtotime($collectionSearchResult['created_time']));
            $collection->title = $collectionSearchResult['name'];
            $collection->description = (!empty($collectionSearchResult['description']))?$collectionSearchResult['description']:'No description available';
            $collection->type = 'vimeo#album';
            $collection->thumb_default = (!empty($collectionSearchResult['pictures']['sizes']['1']['link']))?
              $collectionSearchResult['pictures']['sizes']['1']['link']:$account->thumb_default;
            $collection->thumb_high = (!empty($collectionSearchResult['pictures']['sizes']['2']['link']))?
              $collectionSearchResult['pictures']['sizes']['2']['link']:$account->thumb_default;
            $collection->thumb_low = (!empty($collectionSearchResult['pictures']['sizes']['0']['link']))?
                $collectionSearchResult['pictures']['sizes']['0']['link']:$account->thumb_default;
            $collection->enabled = 0;
            $collection->parent = $account->getID();
            $collection->create(); 
          } else {
            $collection->retrieve();
            $collection->etag = '';
            $collection->publication_date = date('Y-m-d H:i:s',strtotime($collectionSearchResult['created_time']));
            $collection->title = $collectionSearchResult['name'];
            $collection->description = (!empty($collectionSearchResult['description']))?$collectionSearchResult['description']:'No description available';
            $collection->type = 'vimeo#album';
            $collection->thumb_default = (!empty($collectionSearchResult['pictures']['sizes']['1']['link']))?
              $collectionSearchResult['pictures']['sizes']['1']['link']:$account->thumb_default;
            $collection->thumb_high = (!empty($collectionSearchResult['pictures']['sizes']['2']['link']))?
              $collectionSearchResult['pictures']['sizes']['2']['link']:$account->thumb_default;
            $collection->thumb_low = (!empty($collectionSearchResult['pictures']['sizes']['0']['link']))?
                $collectionSearchResult['pictures']['sizes']['0']['link']:$account->thumb_default;
            $collection->parent = $account->getID();
            $collection->update();
          }
          unset($collection);
        }
        return true;
      case 'default':
        return false;
    }
    return false;
  }
  
  public static function getVimeoMediaRecursive($collection, $recurse, $max_depth, $response=false){
    // If either of these conditions is true the this is the last level of recursion
    if($response && (empty($response['body']['paging']['next']) || ($recurse == $max_depth))){
      // BASE CASE REACHED: We have reached the end of the recursion. 
      //dpm('Done');
      return $response['body']['data'];
    } else { // We are still working toward the BASE CASE
      //dpm('Recursing');
      $lib = new \Vimeo\Vimeo($collection->account->provider->auth_field_1, $collection->account->provider->auth_field_2);
      $lib->setToken($collection->account->provider->auth_field_3);
      // Is this the main video list or an album?
      switch($collection->type){
        case 'vimeo#main':
          $url = '/users/' . $collection->getID() . '/videos';
          break;
        case 'vimeo#album':
          $url = '/users/' . $collection->account->getID() . '/albums/'. $collection->getID() . '/videos';
          break;
      }
      if($response) {
        $items = $response['body']['data'];
        $parsed = array();
        parse_str(parse_url($response['body']['paging']['next'],PHP_URL_QUERY), $parsed);
        $params = array('page'=>$parsed['page'],'per_page' => Utility::read_setting('max_results'),'sort'=>'date','direction'=>'desc',);
      } else {
        $items=array();
        $params = array('per_page' => Utility::read_setting('max_results'),);
      }
      $this_response = $lib->request($url, $params);
      
      // Print the debugging information if enabled
     if(Utility::read_setting('debug')){
        $debug = array (
          'URL' => $url,
          'COLLECTION' => $collection,
          'NEXT_PAGE' => $response['body']['paging']['next'],
          'NEXT_PAGE_NUMBER' => $parsed['page'],
          'RECURSION_LEVEL' => $recurse,
          'MAX_RECURSION' => $max_depth,
          'PREVIOUS_RESPONSE' => $response,
          'THIS_RESPONSE' => $this_response,
        );
        dpm($debug);
      }
      
      $recurse++;
      return array_merge($items, MediaEnumeration::getVimeoMediaRecursive($collection, $recurse, $max_depth, $this_response));
    }
  }
  
/** "get" all of the media associated with a vimeo collection from the provider
 * 
 * @param Object $collection
 * @return array Objects of class Media
 */
  public static function getVimeoCollectionMedia($collection){
    $mediaObjects = array();
    $i = 0;
    $items = MediaEnumeration::getVimeoMediaRecursive($collection, 0, Utility::read_setting('max_depth'), false);
    //dpm($items);
    foreach($items as $item){
      list($foo,$bar,$media_id) = explode('/', $item['uri']);
      $mediaObjects[$i] = new Media();
      $mediaObjects[$i]->setID($media_id);
      $mediaObjects[$i]->collection = $collection; // the collection to which this media is linked
      $mediaObjects[$i]->publication_date = date('Y-m-d H:i:s', strtotime($item['created_time']));
      $mediaObjects[$i]->title = $item['name'];
      $mediaObjects[$i]->description = $item['description'];
      $mediaObjects[$i]->type = 'vimeo#video';
      $mediaObjects[$i]->thumb_default = $item['pictures']['sizes'][1]['link'];
      $mediaObjects[$i]->thumb_high = $item['pictures']['sizes'][2]['link'];;
      $mediaObjects[$i]->thumb_low = $item['pictures']['sizes'][0]['link'];;
      $mediaObjects[$i]->latitude = NULL;
      $mediaObjects[$i]->longitude = NULL;
      $mediaObjects[$i]->altitude = NULL;
      $i++;
    }
    return $mediaObjects;
  }
  
  public static function updateVimeoCollectionMedia($collection){
    if(MediaEnumeration::isCollectionEnabled($collection->getID())) {
      $mediaObjects = MediaEnumeration::getVimeoCollectionMedia($collection);
      foreach($mediaObjects as $media){
        //dpm($media);
        if(!MediaEnumeration::checkMedia($media->getID())) {
          $media->create();
        } else {
          $media->update();
        }
      } // foreach
    } // if
    unset($mediaObjects);
    return false;
  }
  
  
  /* *****************************
     YouTube
     *****************************
   */
  
public static function getYoutubePlaylists($account, $collection_id) {
    switch($account->provider->auth_method) {
        case 'oauth2':
          return false;

        case 'apikey':
          $url = $account->provider->api_access_url . '/youtube/v3/playlists';
          $data = array (
              'part'=> 'snippet',
              'channelId' => $collection_id,
              'maxResults' => '50',
              'key' => $account->provider->auth_field_1, 
          );
          $full_url = url($url, array('query' => $data));  
          $collectionSearchResults = drupal_json_decode(drupal_http_request($full_url)->data)['items'];

          return $collectionSearchResults;
    }
    return false;
  }
 
  public static function getYoutubeChannels($account) {
    switch($account->provider->auth_method) {
        case 'oauth2':
          return false;

        case 'apikey':
          $url = $account->provider->api_access_url . '/youtube/v3/channels';
          $data = array (
              'part'=> 'snippet',
              'forUsername' => $account->getID(),
              'maxResults' => '50',
              'key' => $account->provider->auth_field_1, 
          );
          $full_url = url($url, array('query' => $data));  
          $collectionSearchResults = drupal_json_decode(drupal_http_request($full_url)->data)['items'];
          return $collectionSearchResults;
    }
    return false;
  }
 
    public static function updateYoutubeCollections($account){
    switch($account->provider->auth_method) {
      case 'oauth2':
        return false;

      case 'apikey':    
        $collectionSearchResults = MediaEnumeration::getYoutubeChannels($account);
        // Get the playlists and merge them with the channels
        foreach($collectionSearchResults as $collectionSearchResult){
          $playlists = MediaEnumeration::getYoutubePlaylists($account, $collectionSearchResult['id']);
          $collectionSearchResults = array_merge($collectionSearchResults, $playlists);
        }
        
        // Now process each result
        foreach($collectionSearchResults as $collectionSearchResult){ 
          $collection = new Collection();
          $collection->setID($collectionSearchResult['id']);
          if(!MediaEnumeration::checkCollection($collectionSearchResult['id'])) {
            $collection->account = $account;
            $collection->etag = $collectionSearchResult['etag'];
            $collection->publication_date = date('Y-m-d H:i:s',strtotime($collectionSearchResult['snippet']['publishedAt']));
            $collection->title = $collectionSearchResult['snippet']['title'];
            $collection->description = (!empty($collectionSearchResult['snippet']['description']))?$collectionSearchResult['snippet']['description']:'No description available';
            $collection->type = $collectionSearchResult['kind'];
            $collection->thumb_default = $collectionSearchResult['snippet']['thumbnails']['default']['url'];
            $collection->thumb_high = $collectionSearchResult['snippet']['thumbnails']['high']['url'];
            $collection->thumb_low = $collectionSearchResult['snippet']['thumbnails']['medium']['url'];
            $collection->enabled = 0;
            $collection->parent = (!empty($collectionSearchResult['snippet']['channelId']))?$collectionSearchResult['snippet']['channelId']:'';
            $collection->create(); 
          } else {
            $collection->retrieve();
            $collection->etag = $collectionSearchResult['etag'];
            $collection->publication_date = date('Y-m-d H:i:s',strtotime($collectionSearchResult['snippet']['publishedAt']));
            $collection->title = $collectionSearchResult['snippet']['title'];
            $collection->description = (!empty($collectionSearchResult['snippet']['description']))?$collectionSearchResult['snippet']['description']:'No description available';
            $collection->type = $collectionSearchResult['kind'];
            $collection->thumb_default = $collectionSearchResult['snippet']['thumbnails']['default']['url'];
            $collection->thumb_high = $collectionSearchResult['snippet']['thumbnails']['high']['url'];
            $collection->thumb_low = $collectionSearchResult['snippet']['thumbnails']['medium']['url'];
            $collection->parent = (!empty($collectionSearchResult['snippet']['channelId']))?$collectionSearchResult['snippet']['channelId']:'';
            $collection->update();
          }
          unset($collection);
        }
    }
  }
  
  
  /** Recursive function to get all the media on a given youtube channel 
   * 
   * @param string $collection
   * @param int $recurse
   * @param int $max_depth
   * @param string[] $response
   * @return string[] $items
  */
  public static function getYoutubeCollectionMediaRecursive($collection, $recurse, $max_depth, $response=false){
    // If either of these conditions is true the this is the last level of recursion
    if($response && (empty($response['nextPageToken']) || ($recurse == $max_depth))){
      // BASE CASE REACHED: We have reached the end of the recursion. 
      return $response['items'];
      
    } else { // We are still working toward the BASE CASE
      // Is this a playlist or channel?
      
      switch($collection->type){
        case 'youtube#channel':
          $url = $collection->account->provider->api_access_url . '/youtube/v3/search';
          $descriptor = 'channelId';
          break;
        case 'youtube#playlist':
          $url = $collection->account->provider->api_access_url . '/youtube/v3/playlistItems';
          $descriptor = 'playlistId';
          break;
      }
      // build the query
      $max_results = Utility::read_setting('max_results');
      if($response){
      $items = $response['items'];
      $data = array (
        'part'=> 'snippet',
        $descriptor => $collection->getID(),
        'maxResults' => $max_results,
        'pageToken' => $response['nextPageToken'],
        'key' => $collection->account->provider->auth_field_1, 
        );
      } else {
        $items = array();
        $data = array (
        'part'=> 'snippet',
        $descriptor => $collection->getID(),
        'maxResults' => $max_results,
        'key' => $collection->account->provider->auth_field_1, 
        );
      }
      // build the URL
      $full_url = url($url, array('query' => $data));
      
      // Request the data from the server
      $this_response = drupal_json_decode(drupal_http_request($full_url)->data);
      
      // Print the debugging information if enabled
      if(Utility::read_setting('debug')){
        $debug = array (
          'URL' => $full_url,
          'CHANNEL' => $collection,
          'RECURSION_LEVEL' => $recurse,
          'MAX_RECURSION' => $max_depth,
          'PREVIOUS_RESPONSE' => $response,
          'THIS_RESPONSE' => $this_response,
        );
        dpm($debug);
      }
      
      // Increment the recursion depth counter
      $recurse++;
      
      // Recursive Call
      return array_merge($items, MediaEnumeration::getYoutubeCollectionMediaRecursive($collection, $recurse, $max_depth, $this_response)); 
    }
  }
  
   /** get the details for a given piece of youtube media
   * 
   * @param type $media_id
   * @return array
   */
  public static function getYoutubeMediaDetails($media_id) {
      $provider = new Provider();
      $provider->setID('youtube');
      $provider->retrieve();
      switch($provider->auth_method) {
        case 'oauth2':
        /** @ToDo */
        break;
      
        case 'apikey':
          $url = $provider->api_access_url . '/youtube/v3/videos';
          $data = array (
            'part'=> 'recordingDetails',
            'id' => $media_id,
            'key' => $provider->auth_field_1, 
          );
          $full_url = url($url, array('query' => $data));
          $response = drupal_json_decode(drupal_http_request($full_url)->data);
          return (!empty($response['items'][0]))?$response['items'][0]:false;
      }
      return false;
  }
  
    /** "get" all of the media associated with a youtube channel from the provider
   * 
   * @param type $collection_id
   * @return array MediaObjects
   */
  public static function getYoutubeCollectionMedia($collection) {
      $items = array();
      $details = false;
      
      switch($collection->account->provider->auth_method) {
        case 'oauth2':
        /** @ToDo */
        break;
      
        case 'apikey':
          $items = MediaEnumeration::getYoutubeCollectionMediaRecursive($collection, 0, Utility::read_setting('max_depth'), false);
          (Utility::read_setting('debug'))?dpm('Total Items Retrieved: ' . count($items)):'';
          $i=0;
          
          foreach($items as $item){
            $youtubeKind = (!empty($item['id']['kind']))?$item['id']['kind']:$item['kind'];
            $media[$i] = new Media();
            switch($youtubeKind){
              case 'youtube#video':
                $media[$i]->setID($item['id']['videoId']);
                $media[$i]->type = $youtubeKind;
                $details = MediaEnumeration::getYoutubeMediaDetails($item['id']['videoId']);
                break;
              case 'youtube#playlistItem':
                $media[$i]->setID($item['snippet']['resourceId']['videoId']);              
                $media[$i]->type = $item['snippet']['resourceId']['kind'];
                $details = MediaEnumeration::getYoutubeMediaDetails($item['snippet']['resourceId']['videoId']);
                break;
            }
            // Private media has an empty response.
            // If the media is private abort the loop.
           if($details){ 
              $media[$i]->collection = $collection; // the channel to which this media is linked
              $media[$i]->publication_date = date('Y-m-d H:i:s',strtotime($item['snippet']['publishedAt']));
              $media[$i]->title = $item['snippet']['title'];
              $media[$i]->description = $item['snippet']['description'];
              $media[$i]->thumb_default = $item['snippet']['thumbnails']['default']['url'];
              $media[$i]->thumb_high = $item['snippet']['thumbnails']['high']['url'];
              $media[$i]->thumb_low = $item['snippet']['thumbnails']['medium']['url'];
              if(!empty($details['recordingDetails']['location'])){
                  $media[$i]->latitude = (!empty($details['recordingDetails']['location']['latitude']))?$details['recordingDetails']['location']['latitude']:'';
                  $media[$i]->longitude = (!empty($details['recordingDetails']['location']['longitude']))?$details['recordingDetails']['location']['longitude']:'';
                  $media[$i]->altitude = (!empty($details['recordingDetails']['location']['altitude']))?$details['recordingDetails']['location']['altitude']:'';
                } else {
                  $media[$i]->latitude = NULL;
                  $media[$i]->longitude = NULL;
                  $media[$i]->altitude = NULL;
                }
              $i++;
            } else { 
              unset($media[$i]);  
            }
          }
          return $media;
      }
      return false;
  }

  /** Update the collection's media in the database
   * 
   * @param type $collection
   */
  public static function updateYoutubeCollectionMedia($collection){
    /** NOTE: Sometimes channel['id'] is a string with the ID and sometimes it's an array */
    // If then channel is enabled then perform the update/create operations
    if(MediaEnumeration::isCollectionEnabled($collection->getID())) {
      $mediaObjects = MediaEnumeration::getYoutubeCollectionMedia($collection);
      //dpm($collectionMediaResults);
      foreach($mediaObjects as $media){
        //dpm($media);
        if(!MediaEnumeration::checkMedia($media->getID())) {
          $media->create();
        } else {
          $media->update();
        }
      } // foreach
    } // if
    unset($mediaObjects);
  } // function
  
}
