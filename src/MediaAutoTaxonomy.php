<?php
/** A class of static functions to create/update/delete Taxonomy terms
*/
/**				  
 * @file Taxonomy.php
 * @name Taxonomy 
 * @author Susanne Coates
 * @copyright (c)2015 University of Maryland
 * @todo
 */
/*
 This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License version 2
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


require_once 'Utility.php';

interface MediaAutoTaxonomyInterface {
  public function MediaAutoTaxonomy();
  public function create();
  public function delete();
}

final class MediaAutoTaxonomy implements MediaAutoTaxonomyInterface {
  private $vocabulary_name;
  private $terms;
  private $descriptions;
  
  public function MediaAutoTaxonomy(){
    $this->vocabulary_name = 'media_auto_tags';
    $this->terms = array (
      'Featured',
      'In the News',
    );
    $this->descriptions = array (
      'Items to be displayed in the _Featured_ block',
      'Items to be displayed in the _In the News_ block',
    );
  }
  
  public function create(){
    $vocab = (object) array(
      'name' => 'Media Auto Tags',
      'description' => 'This vocabulary provides tags for categorizing nodes created by the Media Auto module',
      'machine_name' => 'media_auto_tags',
    );  
    taxonomy_vocabulary_save( $vocab );
    
    $vocab = taxonomy_vocabulary_machine_name_load( $this->vocabulary_name );
    
    for ( $i=0; $i<count($terms); $i++ ){
      $term = (object) array(
      'name' => $terms[$i],
      'description' => $descriptions[$i],
      'vid' => $vocab->vid,
      );
      taxonomy_term_save( $term );
    }
  }
  
  public function delete(){
    $vocab = taxonomy_vocabulary_machine_name_load( $this->vocabulary_name );
    taxonomy_vocabulary_delete( $vocab->vid );
  }
  
} 


