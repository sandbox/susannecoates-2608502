<?php
/** The Media object
*/
/**
 * @file Media.php
 * @name Media 		  
 * @author Susanne Coates
 * @copyright (c)2015 University of Maryland
 * @todo
 */
/*
 This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License version 2
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
require_once 'Provider.php';
require_once 'Account.php';
require_once 'Collection.php';

interface MediaInterface {
  public function setID($id);
  public function getID();
  //CRUD functions
  public function create();
  public function retrieve();
  //public static function updateCompare($mediaObject1, $mediaObject2);
  public function update();
  //public function updateWith($mediaObject);
  public function delete($deleteNode);
  // Object validation
  public function validate();
  public static function checkMedia($media_id);
}

class Media implements MediaInterface{
  private $media_id;
  public $collection; // the collection to which this media is linked
  public $node;
  public $publication_date;
  public $title;
  public $description;
  public $body;
  public $url;
  public $type;
  public $status;
  public $hide;
  public $language;
  public $tags;
  public $thumb_default;
  public $thumb_high;
  public $thumb_low;
  public $latitude;
  public $longitude;
  public $altitude;
  
  // Class Initialzer
  public function Media() {
    $this->media_id = -1;
    $this->collection = new Collection();
    $this->node = new stdClass();
    $this->publication_date = '2000-01-01 23:00:00';
    $this->title = '';
    $this->description = '';
    $this->body = '';
    $this->url = '';
    $this->type = '';
    $this->status = 1;
    $this->hide = 0;
    $this->language = 'und';
    $this->tags = array();
    $this->thumb_default = '';
    $this->thumb_high = '';
    $this->thumb_low = '';
    $this->latitude = 0.00000000000;
    $this->longitude = 0.00000000000;
    $this->altitude = 0.00000000000;
  }
  
  public function setID($id){
    $this->media_id = $id;
  }
  
  public function getID() {
    return $this->media_id;
  }
  
   /** Stores a new media object to the database
    * 
    */
  public function create() {
    $this->node->title = $this->title;
    $this->node->type = 'media'; // content type = media
    node_object_prepare($this->node);
    $this->node->language = $this->language;
    $this->node->uid = 0; // the system owns these nodes
    $this->node->status = $this->status; //(1 or 0): published or not
    $this->node->promote = 0; //(1 or 0): promoted to front page
    $this->node->comment = 0; // 0 = comments disabled, 1 = read only, 2 = read/write
    node_save($this->node);
    
    // Since $collection may not contain a complete object at this point
    // we get the provider by parsing the provider#type string from the Query.
    //list($provider,$type) = explode('#',$this->type);
    // Add the managed file and get the fid
    switch($this->collection->account->provider->getID()){
      case 'youtube':
        $file = (object) array(
          'uid' => 0,
          'filename' => $this->title,
          'uri' => 'youtube://v/' . $this->media_id,
          'filemime' => 'video/youtube',
          'filesize' => 0,
          'status' => 1,
          'type' => 'video',
        );
      break;
      case 'vimeo':
        $file = (object) array(
          'uid' => 0,
          'filename' => $this->title,
          'uri' => 'vimeo://v/' . $this->media_id,
          'filemime' => 'video/vimeo',
          'filesize' => 0,
          'status' => 1,
          'type' => 'video',
        );
        break;
    }
    $fid = file_save($file);
    
    // now add the entries to the nodes field collection. 
    $this->node->field_mid[LANGUAGE_NONE][0]['value'] = $this->media_id;
    $this->node->field_publication_date[LANGUAGE_NONE][0]['value'] = date('Y-m-d H:i:s', strtotime($this->publication_date));
    $this->node->field_feed_item_description[LANGUAGE_NONE][0]['value'] = $this->description;
    $this->node->field_media_url[LANGUAGE_NONE][0]['fid'] = $fid->fid;
    $this->node->field_media_url[LANGUAGE_NONE][0]['display'] = 1;
    $this->node->field_media_url[LANGUAGE_NONE][0]['description'] = $this->description;
    $this->node->field_thumbnail_default[LANGUAGE_NONE][0]['value'] = $this->thumb_default;
    $this->node->field_thumbnail_large[LANGUAGE_NONE][0]['value'] = $this->thumb_high;
    $this->node->field_thumbnail_small[LANGUAGE_NONE][0]['value'] = $this->thumb_low;
    $this->node->field_channel[LANGUAGE_NONE][0]['value'] = $this->collection->getID();
    $this->node->field_tags[LANGUAGE_NONE][0]['tid'] = '0';

    node_save($this->node);
    $nid = $this->node->nid;

    $fields_array = array (
      'media_id' => $this->media_id,
      'collection_id' => $this->collection->getID(),
      'nid' => $this->node->nid,
      'latitude' => $this->latitude,
      'longitude' => $this->longitude,
      'altitude' => $this->altitude,
    ); 

   Utility::insert_row('media_auto_media_index', $fields_array);
   // Generate some logging information
   if(Utility::read_setting('log')){
    $messagetemp = 'Added YouTube ID: ' . $this->media_id . ', Title: ' . $this->title ;
    watchdog('Media Auto', $messagetemp, NULL, WATCHDOG_DEBUG);
   }
  }
   
  /** returns a fully loaded MediaObject for the given media_id
   * 
   * @return boolean
   */
  public function retrieve() {
    $query = db_select('media_auto_media_index', 'n')
	    ->fields('n', array('media_id','collection_id','nid','hide','latitude','longitude','altitude'))
		  ->condition('media_id',$this->media_id,'=')
	    ->execute();
		$result = $query->fetch();
    if(!empty($result->media_id)) {
      $this->collection->setID($result->collection_id);
      $this->collection->retrieve();
      $this->node = node_load($result->nid);
      $this->title = $this->node->title;
      $this->publication_date = $this->node->field_publication_date['und'][0]['value'];
      $this->description = (!empty($this->node->field_feed_item_description['und'][0]['value']))? $this->node->field_feed_item_description['und'][0]['value']: '';
      $this->body = (!empty($this->node->body['und'][0]['value']))? $this->node->body['und'][0]['value']:'';
      $this->url = $this->collection->account->provider->watch_url . '/' . $this->node->field_mid['und'][0]['value'];
      $this->type = $this->node->type;
      $this->status = $this->node->status;
      $this->hide = $result->hide;
      $this->language = $this->node->language;   
      $this->tags = ($this->node->field_tags[LANGUAGE_NONE][0]['tid'])?explode( ',' , $this->node->field_tags[LANGUAGE_NONE][0]['tid']):'';
      $this->thumb_default = $this->node->field_thumbnail_default['und'][0]['value'];
      $this->thumb_high = $this->node->field_thumbnail_large['und'][0]['value'];
      $this->thumb_low = $this->node->field_thumbnail_small['und'][0]['value'];
      $this->latitude = $result->latitude;
      $this->longitude = $result->longitude;
      $this->altitude = $result->altitude;
      return true;
    } else {
      return false;
    }
  }
  
  public function update(){
    // get the existing values from the table
    $query = db_select('media_auto_media_index', 'mami')
	    ->fields('mami', array('media_id','collection_id','nid','hide','latitude','longitude','altitude',))
		  ->condition('media_id',$this->media_id,'=')
	    ->execute();
		$result = $query->fetch();
    $node = node_load($result->nid);
    
    $node->title = ($this->title <> $node->title)?$this->title:$node->title;
    $node->field_publication_date['und'][0]['value'] = 
      ($this->publication_date <> $node->field_publication_date['und'][0]['value'])?
        $this->publication_date:
        $node->field_publication_date['und'][0]['value'];

    $node->field_feed_item_description['und'][0]['value'] = 
      ($this->description <> $node->field_feed_item_description['und'][0]['value'])?
        $this->description:
        $node->field_feed_item_description['und'][0]['value'];
    
    $node->status = (boolean)$this->status && (boolean)!$result->hide;
    
    $node->language = ($this->language <> $node->language)? $this->language:$node->language;


    //($this->hide <> $result->hide)? Utility::update_cell('media_auto_media_index', 'media_id', $this->media_id, 'hide',$this->hide ):'';
    
    $node->field_thumbnail_default['und'][0]['value'] = 
      ($this->thumb_default <> $node->field_thumbnail_default['und'][0]['value'])?
        $this->thumb_default:
        $node->field_thumbnail_default['und'][0]['value'];
    
    $node->field_thumbnail_large['und'][0]['value'] = 
      ($this->thumb_high <> $node->field_thumbnail_large['und'][0]['value'])?
        $this->thumb_high:
        $node->field_thumbnail_large['und'][0]['value'];
    
    $node->field_thumbnail_small['und'][0]['value'] = 
      ($this->thumb_low <> $node->field_thumbnail_small['und'][0]['value'])?
        $this->thumb_low:
        $node->field_thumbnail_small['und'][0]['value'];
    
    ($this->latitude <> $result->latitude)? Utility::update_cell('media_auto_media_index', 'media_id', $this->media_id, 'latitude',$this->latitude ):'';
    ($this->longitude <> $result->longitude)? Utility::update_cell('media_auto_media_index', 'media_id', $this->media_id, 'longitude',$this->longitude ):'';
    ($this->altitude <> $result->altitude)? Utility::update_cell('media_auto_media_index', 'media_id', $this->media_id, 'altitude',$this->altitude ):'';
    node_save($node);
    
    // Generate some logging information
    if(Utility::read_setting('log')){
      $messagetemp = 'Updated ID: ' . $this->media_id . ', Title: ' . $this->title ;
      watchdog('Media Auto',$messagetemp,NULL,WATCHDOG_DEBUG);
    }

  }
  
     /** Compare the relevant fields of two media objects and return TRUE if they are identical
   *   This is used to determine if Media->update should be called
   * @param Object $mediaObject1
   * @param Object $mediaObject2
   * @return Boolean
   */
  /*
  public static function updateCompare($mediaObject1, $mediaObject2) {
    return ($mediaObject1->title == $mediaObject2->title) &&
      (date('Y-m-d H:i:s', strtotime($mediaObject1->publication_date)) == date('Y-m-d H:i:s', strtotime($mediaObject2->publication_date))) &&
      ($mediaObject1->description == $mediaObject2->description) &&
      ($mediaObject1->thumb_default == $mediaObject2->thumb_default) &&
      ($mediaObject1->thumb_high == $mediaObject2->thumb_high) &&
      ($mediaObject1->thumb_low == $mediaObject2->thumb_low);
  } 
*/
  
  /** Update the fields in the current media object with those of the passed object
   *  if the fields differ. this does a field-by-field comparison and update.
   * 
   */
  /*
  public function updateWith($mediaObject) {
    // Get the current media object stored in the DB
    $dbMedia = new Media();
    $dbMedia->setID($mediaObject->media_id);
    $dbMedia->retrieve();
 */
    
    /** Compare each field and update if it has changed.
     *  Note: we do not update the media ID, node id, and body. 
     *  Media ID and nid remain the same until the media and node are deleted.
     *  The body is edited via the Node->edit tab and is not auto-updated.
     */
    //$dbMedia->media_id;
  /*
    ($dbMedia->collection->getID() <> $mediaObject->collection->getID())? Utility::update_cell('media_auto_media_index', 'media_id', $dbMedia->media_id, 'collection_id', $mediaObject->collection->collection_id):'';
    $dbMedia->node->field_publication_date['und'][0]['value'] = 
        ($dbMedia->publication_date <> $mediaObject->publication_date)? $mediaObject->publication_date:$dbMedia->publication_date;
    $dbMedia->node->title = 
        ($dbMedia->title <> $mediaObject->title)?$mediaObject->title:$dbMedia->title;
    $dbMedia->node->field_feed_item_description['und'][0]['value'] = 
        ($dbMedia->description <> $mediaObject->description)? $mediaObject->description : $dbMedia->description;
    $dbMedia->node->status = 
        ($dbMedia->status <> $mediaObject->status)? $mediaObject->status : $dbMedia->status;
    $dbMedia->hide = 
        ($dbMedia->hide <> $mediaObject->hide)? Utility::update_cell('media_auto_media_index', 'media_id', $dbMedia->media_id, 'hide', $mediaObject->hide):'';
    $dbMedia->node->language = 
        ($dbMedia->language <> $mediaObject->language)? $mediaObject->language : $dbMedia->language;
    
    $dbMedia->node->field_tags = (!empty($mediaObject->tags))?array('und' => $mediaObject->tags):array();
    
    $dbMedia->node->field_thumbnail_default['und'][0]['value'] = 
        ($dbMedia->thumb_default <> $mediaObject->thumb_default)? $mediaObject->thumb_default : $dbMedia->thumb_default;
    $dbMedia->node->field_thumbnail_large['und'][0]['value'] = 
        ($dbMedia->thumb_high <> $mediaObject->thumb_high)? $mediaObject->thumb_high : $dbMedia->thumb_high;
    $dbMedia->node->field_thumbnail_small['und'][0]['value'] = 
        ($dbMedia->thumb_low <> $mediaObject->thumb_low)? $mediaObject->thumb_low : $dbMedia->thumb_low;
    
    ($dbMedia->latitude <> $mediaObject->latitude)?
      Utility::update_cell('media_auto_media_index', 'media_id', $dbMedia->media_id, 'latitude', $mediaObject->latitude):'';
    ($dbMedia->longitude <> $mediaObject->longitude)?
      Utility::update_cell('media_auto_media_index', 'media_id', $dbMedia->media_id, 'longitude', $mediaObject->longitude):'';
    ($dbMedia->altitude <> $mediaObject->altitude)?
      Utility::update_cell('media_auto_media_index', 'media_id', $dbMedia->media_id, 'altitude', $mediaObject->altitude):'';
    node_save($dbMedia->node);
    
    // Generate some logging information
    if(Utility::read_setting('log')){
      $messagetemp = 'Updated ID: ' . $this->media_id . ', Title: ' . $this->title ;
      watchdog('Media Auto',$messagetemp,NULL,WATCHDOG_DEBUG);
    }
  }
   * 
   */
  
  /** Check a single youtube search result item to see if it already exists then either create or update it.
   * 
   * @param type $collectionMediaResult
   * @param type $collection
   */
  /*
  public static function youtubeCheckAndUpdate($collectionMediaResult, $collection){
    $mediaID = MediaEnumeration::parseMediaIDFromSearchResults($collectionMediaResult, $collection->type);
    if(!empty($mediaID)) {

      // Check to see if the media is already in the database
      if(Media::checkMedia($mediaID)){
        // build the two media objects for comparison
        // media1 contains the existing media in the DB
        $media1 = new Media();
        $media1->setID($mediaID);
        $media1->retrieve();

        // media2 contains the media data fields (below) obtained from the provider and is not a complete Media object.
        $media2 = new Media();
        // set an populate the collection->account->provider hierarchy
        $media2->collection->setID($collection_id);
        $media2->collection->retrieve();

        $media2->setID($mediaID);
        $media2->title = $collectionMediaResult['snippet']['title'];
        $media2->publication_date = date('Y-m-d H:i:s', strtotime($collectionMediaResult['snippet']['publishedAt']));
        $media2->description = $collectionMediaResult['snippet']['description'];
        $media2->thumb_default = $collectionMediaResult['snippet']['thumbnails']['default']['url'];
        $media2->thumb_high = $collectionMediaResult['snippet']['thumbnails']['high']['url'];
        $media2->thumb_low = $collectionMediaResult['snippet']['thumbnails']['medium']['url'];

        //check if media needs to be updated. If is does, update media1 with media2.
        (!Media::updateCompare($media1,$media2))?$media1->update($media2):'';

        unset($media1); unset($media2);

      } else {
        //add media to table
        $media = new Media();
        $media->media_id = $mediaID;
        $media->collection = new Collection();
        $media->collection->setID($collection_id);
        $media->collection->retrieve();

        $mediaResult = MediaEnumeration::getYoutubeMediaDetails($media->media_id);
        $media->publication_date = date('Y-m-d H:i:s', strtotime($mediaResult['snippet']['publishedAt']));
        $media->title = $mediaResult['snippet']['title'];
        $media->description = $mediaResult['snippet']['description'];
        $media->body = '';
        $media->url = 'https://youtu.be/' . $mediaID;
        $media->type = $collectionMediaResult['id']['kind'];
        $media->status = 1;
        $media->hide = 0;
        $media->language = 'und';
        $media->thumb_default = $collectionMediaResult['snippet']['thumbnails']['default']['url'];
        $media->thumb_high = $collectionMediaResult['snippet']['thumbnails']['high']['url'];
        $media->thumb_low = $collectionMediaResult['snippet']['thumbnails']['medium']['url'];
        $media->create();

        unset($media);
      }
    }
  }
   */
  
  /** Delete the media from the system and (optionally) delete the node.
   * @param int $deleteNode
   */
  public function delete($deleteNode = 0){
    Utility::drop_row('media_auto_media_index','media_id',$this->getID());
    if($deleteNode) { node_delete($this->node->nid); }
    
    // Generate some logging information
    if(Utility::read_setting('log')){
     $messagetemp = 'Deleted Media ID: ' . $this->media_id . ', Title: ' . $this->title . ', Delete Node?: ' . $deleteNode;
     watchdog('Media Auto', $messagetemp, NULL, WATCHDOG_DEBUG);
    }
  }
  
  /** Validate the Media Object
   *  Check that the resources pointed to by the relevent fields in the object are valid (do not return a 404, etc.)
   */
  public function validate() {
  
  }
  
  /** Check if a peice of media is already in the media_index table
   * 
   * @param string $media_id
   * @return boolean
   */
  public static function checkMedia($media_id){
    $query = db_select('media_auto_media_index', 'n')
	    ->fields('n', array('media_id'))
		  ->condition('media_id',$media_id,'=')
	    ->execute();
		$result = $query->fetch();
    return (!empty($result->media_id));
  }
}